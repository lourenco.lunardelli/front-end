﻿using Models.ServiceLayer.Documents;

namespace Domain.SAP.Picking
{
    public static class PickingSpecification
    {
        public static bool PickingNotExists(SAPDocuments documentsSAP)
        {
            return documentsSAP.DocEntry == 0;
        }
    }
}
