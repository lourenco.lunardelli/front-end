﻿using Domain.SAP.Order;
using Domain.SAP.Picking.Interfaces;
using Hangfire;
using Infrastructure.Clients.GrayLog;
using Models;
using Models.Roter;
using Models.ServiceLayer.Documents;
using Models.Verweis;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.SAP.Picking
{
    public class PicklistService : InjectionBase, IPicklistService
    {
        public PicklistService(IServiceProvider serviceProvider) : base(serviceProvider) { }


        [Queue("picking")]
        public async Task GeneratePickList(string OrderNumber, int ecommerceID)
        {
            NewOrderModel orderModel = await BahngleisFacade.OrderRepo.GetOrderModelAsync(OrderNumber, ecommerceID);
            orderModel.Documents.OrderByDescending(s => s.DocumentID).Last().JobSucceeded = true;

            try
            {
                NewEcommerceModel ecommerceModel = await VerweisFacade.Repo.GetEcommerce(orderModel.EcommerceID);
                SAPDocuments documentsSAP = OrderFacade.SAPRepo.GetDocument(OrderNumber);

                if (!await OrderSpecification.OrderIsCanceled(ecommerceModel.Platform.Name, orderModel, documentsSAP, BahnMagentoFacade))
                {
                    if (PickingSpecification.PickingNotExists(documentsSAP) && !orderModel.Shipping.IsDropShipping)
                    {
                        await CreatePicklist(documentsSAP.DocEntry);

                        await EnqueueInvoice(orderModel, "");
                        await BahngleisFacade.OrderRepo.UpdateInDb(orderModel);
                    }
                }
                else
                {
                    orderModel.Status = await BahngleisFacade.OrderRepo.GetStatus("canceled");
                    await BahngleisFacade.OrderRepo.UpdateInDb(orderModel);
                }
            }
            catch (Exception exc)
            {
                GrayLogClient.GetLogger().Error(exc, "SAP - Error adding OrderModel", orderModel);
                await BahngleisFacade.OrderService.JobFailed(orderModel, exc);

                throw exc;
            }
        }

        public async Task<PickingModel> CreatePicklist(int orderEntry)
        {
            return await Infrastructure.Facades.Roter.HttpClient.PostAsync<PickingModel>(null, $"Picking/CreatePicking({orderEntry})");
        }

        private async Task EnqueueInvoice(NewOrderModel orderModel, string entry)
        {
            //TODO: descomentar quando fizer a invoice
            //string jobID = BackgroundJob.Enqueue(() => InvoiceFacade.Service.GenerateInvoice(orderModel.Number, orderModel.EcommerceID));

            orderModel.Documents.Add(new NewDocumentsModel
            {
                DocumentType = await BahngleisFacade.OrderRepo.GetDocumentType("Nota Fiscal"),
                //JobID = jobID,
                Entry = entry,
                JobSucceeded = false
            });
        }
    }
}
