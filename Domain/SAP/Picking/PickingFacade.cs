using Domain.SAP.Picking.Interfaces;
using System;

namespace Domain.SAP.Picking
{
    public class PickingFacade : IPickingFacade
    {
        private readonly IServiceProvider _serviceProvider;
        private PicklistService _picklistService;
        
        public PickingFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IPicklistService Service
        {
            get
            {
                if (_picklistService == null)
                    _picklistService = new PicklistService(_serviceProvider);
                return _picklistService;
            }
        }
    }
}