namespace Domain.SAP.Picking.Interfaces
{
    public interface IPickingFacade
    {
        IPicklistService Service { get; }
    }
}