﻿using Models.Roter;
using System.Threading.Tasks;

namespace Domain.SAP.Picking.Interfaces
{
    public interface IPicklistService
    {
        Task<PickingModel> CreatePicklist(int orderEntry);
        Task GeneratePickList(string OrderNumber, int ecommerceID);
    }
}