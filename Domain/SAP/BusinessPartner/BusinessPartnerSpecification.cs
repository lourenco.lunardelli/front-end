﻿using Models;
using Models.ServiceLayer.BusinessPartner;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.SAP.BusinessPartner
{
    public static class BusinessPartnerSpecification
    {
        public static bool IsCNPJ(string cpfCnpj)
        {
            if (string.IsNullOrEmpty(cpfCnpj))
                return false;

            string CNPJ = cpfCnpj.Replace(".", "");
            CNPJ = CNPJ.Replace("/", "");
            CNPJ = CNPJ.Replace("-", "");
            int[] digitos, soma, resultado;
            int nrDig;
            string ftmt;
            bool[] CNPJOk;
            ftmt = "6543298765432";
            digitos = new int[14];
            soma = new int[2];
            soma[0] = 0; soma[1] = 0;
            resultado = new int[2];
            resultado[0] = 0;
            resultado[1] = 0;
            CNPJOk = new bool[2];
            CNPJOk[0] = false;
            CNPJOk[1] = false;
            try
            {
                for (nrDig = 0; nrDig < 14; nrDig++)
                {
                    digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                    if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                    if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
                }
                for (nrDig = 0; nrDig < 2; nrDig++)
                {
                    resultado[nrDig] = (soma[nrDig] % 11);
                    if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                    else CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
                }
                return (CNPJOk[0] && CNPJOk[1]);
            }
            catch
            {
                return false;
            }
        }

        public static bool IsCPF(string vrCPF)
        {
            if (string.IsNullOrEmpty(vrCPF))
                return false;

            string valor = vrCPF.Replace(".", "");
            valor = valor.Replace("-", "");
            if (valor.Length != 11)
                return false;

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
                if (valor[i] != valor[0]) igual = false;
            if (igual || valor == "12345678909")
                return false; int[] numeros = new int[11];
            for (int i = 0; i < 11; i++) numeros[i] = int.Parse(valor[i].ToString());
            int soma = 0; for (int i = 0; i < 9; i++) soma += (10 - i) * numeros[i];
            int resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                    return false;
            }
            else if (numeros[9] != 11 - resultado)
                return false;

            soma = 0;
            for (int i = 0; i < 10; i++) soma += (11 - i) * numeros[i];
            resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                    return false;
            }
            else if (numeros[10] != 11 - resultado)
                return false;
            return true;
        }

        public static bool IsTaxPayer(BusinessPartnerModel businessPartner)
        {
            return IsCNPJ(businessPartner.Password) && HasStateRegistry(businessPartner);
        }

        public static bool IsTaxPayer(NewCustomerModel customer)
        {
            return IsCNPJ(customer.TaxIdentification) && HasStateRegistry(customer);
        }

        public static bool ShouldPayST(BusinessPartnerModel businessPartner)
        {
            List<string> resellerCnaes = new List<string>
            {
                "2610-8/00",
                "2622-1/00",
                "2632-9/00",
                "2640-0/00",
                "2651-5/00",
                "2670-1/02",
                "2759-7/01",
                "2790-2/99",
                "4647-8/01",
                "4647-8/02",
                "4649-4/01",
                "4649-4/02",
                "4649-4/07",
                "4651-6/01",
                "4651-6/02",
                "4713-0/01",
                "4713-0/02",
                "4713-0/03",
                "4751-2/01",
                "4753-9/00",
                "4761-0/03",
                "4762-8/00",
                "4789-0/07",
                "4789-0/08",
                "6190-6/99",
                "6209-1/00",
                "9511-8/00",
                "9512-6/00",
                "9521-5/00",
                "9529-1/99"
            };

            var bpAddress = businessPartner.BPAddresses.FirstOrDefault(s => s.AddressType == "PAGAMENTO");

            if ((bpAddress == null || bpAddress.State == "PR" && bpAddress.State == "RS")
                && IsTaxPayer(businessPartner) && businessPartner.BPFiscalTaxIDCollection.Any(s => s.CNAECode != null && resellerCnaes.Contains(s.CNAECode.ToString())))
                return true;

            return false;

        }

        public static bool HasStateRegistry(BusinessPartnerModel businessPartner)
        {
            return businessPartner.BPFiscalTaxIDCollection.Any(s => !String.IsNullOrEmpty(s.TaxId1)) &&
                   businessPartner.BPFiscalTaxIDCollection.Any(s => s.TaxId1.Trim().ToLower() != "isento");
        }

        public static bool HasStateRegistry(NewCustomerModel customer)
        {
            return !string.IsNullOrEmpty(customer.Reseller.StateRegistry) && customer.Reseller.StateRegistry.Trim().ToLower() != "isento";
        }

        public static bool ShouldAddPaymentMethod(List<BusinessPartnerPaymentMethods> businessPartnerPayments, string paymentMethod)
        {
            return !businessPartnerPayments.Any(s => s.PaymentMethodCode == paymentMethod);
        }
    }
}
