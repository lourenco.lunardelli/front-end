using Models;
using Models.ServiceLayer.BusinessPartner;
using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Domain.SAP.BusinessPartner
{
    public static class BusinessPartnerHelper
    {
        public const int _taxPayerGroupCode = 100;
        public const int _finalCustomerGroupCode = 102;

        public const string _taxPayerIndicator = "1";
        public const string _notTaxPayerIndicator = "9";

        public const int _sellToDemand = 32;
        public const int _resale = 25;
        public const int _finalConsumer = 29;

        public static BusinessPartnerAddress ParseAddress(NewAddressModel address, string cardCode, string ieIndicator, string county)
        {
            return new BusinessPartnerAddress
            {
                BPCode = cardCode,
                RowNum = address.AddressType.ToUpper() == "PAGAMENTO" ? 0 : 1,
                AddressName = address.AddressType.ToUpper(),
                AddressType = address.AddressType.ToUpper() == "PAGAMENTO" ? "bo_BillTo" : "bo_ShipTo",
                Street = address.Street.Length > 50 ? address.Street.Substring(0, 50) : address.Street,
                StreetNo = address.Number,
                ZipCode = address.ZipCode,
                Block = address.District,
                City = address.City,
                State = address.UF,
                Country = address.Country,
                AddressName2 = address.Name.Length > 50 ? address.Name.Substring(0, 50) : address.Name,
                BuildingFloorRoom = !string.IsNullOrEmpty(address.Complement) ? address.Complement : null,
                County = county,
                U_SKILL_indIEDest = ieIndicator
            };
        }

        public static BusinessPartnerPaymentMethods ParsePayment(string payment)
        {
            return new BusinessPartnerPaymentMethods
            {
                PaymentMethodCode = payment
            };
        }


        //---------------------------------------------AUXILIARIES---------------------------------------------
        public static void CustomerTreatment(NewCustomerModel customer, BusinessPartnerModel businessPartner)
        {
            customer.Name = customer.Name.Length > 40 ? customer.Name.Substring(0, 40) : customer.Name;
            customer.Reseller.CorporateName = (!string.IsNullOrEmpty(customer.Reseller.CorporateName) && customer.Reseller.CorporateName.Length > 100) ? customer.Reseller.CorporateName.Substring(0, 100) : customer.Reseller.CorporateName;
        }

        public static string TrimmString(string shippingAddressStreetAdditional, int lenght)
        {
            if (shippingAddressStreetAdditional.Length < lenght)
            {
                return shippingAddressStreetAdditional;
            }

            return shippingAddressStreetAdditional.Substring(0, lenght - 1);
        }

        public static string Format(string taxVat)
        {
            string taxVatUnformatted = Unformat(taxVat);

            if (BusinessPartnerSpecification.IsCNPJ(taxVatUnformatted))
                return Convert.ToUInt64(taxVatUnformatted).ToString(@"00\.000\.000\/0000\-00");

            else if (BusinessPartnerSpecification.IsCPF(taxVatUnformatted))
                return Convert.ToUInt64(taxVatUnformatted).ToString(@"000\.000\.000\-00");

            else
                throw new Exception($"CPF/CNPJ {taxVat} inválido");
        }

        public static string Unformat(string taxVat)
        {
            if (string.IsNullOrEmpty(taxVat)) return taxVat;

            taxVat = new string(taxVat.Where(char.IsDigit).ToArray());

            return taxVat;
        }

        public static string RemoveAccents(string word)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = word.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }
    }
}