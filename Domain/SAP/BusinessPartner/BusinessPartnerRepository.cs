﻿using Domain.SAP.BusinessPartner.Interfaces;
using Infrastructure.Facades;
using Models.ServiceLayer.BusinessPartner;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.SAP.BusinessPartner
{
    public class BusinessPartnerRepository : IBusinessPartnerRepository
    {
        //------------------------------GET------------------------------
        public async Task<BusinessPartnerModel> GetBusinessPartner(string TaxVat)
        {

            var businessPartner = await ServiceLayer.HttpClient.GetByQueryAsync<BusinessPartnerModel>($"$filter=Password eq '{BusinessPartnerHelper.Format(TaxVat)}' and CardType eq 'cCustomer'");

            if (businessPartner is null)
                businessPartner = await ServiceLayer.HttpClient.GetByQueryAsync<BusinessPartnerModel>($"$filter=Password eq '{BusinessPartnerHelper.Unformat(TaxVat)}' and CardType eq 'cCustomer'");

            if (businessPartner is null)
            {
                businessPartner = new BusinessPartnerModel
                {
                    BPFiscalTaxIDCollection = new List<BusinessPartnerFiscalTaxIdCollection>(),
                    BPAddresses = new List<BusinessPartnerAddress>(),
                    BPPaymentMethods = new List<BusinessPartnerPaymentMethods>()
                };
            }

            return businessPartner;
        }

        //------------------------------POST------------------------------
        public async Task<BusinessPartnerModel> AddBusinessPartner(BusinessPartnerModel businessPartner)
        {
            businessPartner.BilltoDefault = "PAGAMENTO";
            businessPartner.ShipToDefault = "ENTREGA";
            businessPartner.LanguageCode = 29; //Português (BR)
            businessPartner.Series = 71;
            businessPartner.Currency = "R$";

            return await ServiceLayer.HttpClient.PostAsync(businessPartner);
        }

        //------------------------------PATCH------------------------------
        public async Task UpdateBusinessPartner(BusinessPartnerModel businessPartner)
        {
            businessPartner.BilltoDefault = "PAGAMENTO";
            businessPartner.ShipToDefault = "ENTREGA";
            businessPartner.LanguageCode = 29; //Português (BR)

            //First need remove BP Address to update BP Fiscal otherwise SL return error
            var emptyBp = new BusinessPartnerModel()
            {
                BPAddresses = new List<BusinessPartnerAddress>()
            };

            await ServiceLayer.HttpClient.PatchAsync(businessPartner.CardCode, emptyBp);
            await ServiceLayer.HttpClient.PatchAsync(businessPartner.CardCode, businessPartner);
        }
    }
}
