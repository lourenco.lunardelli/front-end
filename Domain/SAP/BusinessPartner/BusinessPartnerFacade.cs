﻿using Domain.SAP.BusinessPartner.Interfaces;
using System;

namespace Domain.SAP.BusinessPartner
{
    public class BusinessPartnerFacade : IBusinessPartnerFacade
    {
        private BusinessPartnerService _service;
        private BusinessPartnerRepository _repo;

        private readonly IServiceProvider _serviceProvider;

        public BusinessPartnerFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IBusinessPartnerService Service
        {
            get
            {
                if (_service == null)
                    _service = new BusinessPartnerService(_serviceProvider);
                return _service;
            }
        }
        public IBusinessPartnerRepository Repo
        {
            get
            {
                if (_repo == null)
                    _repo = new BusinessPartnerRepository();
                return _repo;
            }
        }
    }
}
