﻿using Domain.Nuuk;
using Domain.SAP.BusinessPartner.Interfaces;
using Domain.SAP.Order;
using Flurl.Http;
using Flurl.Http.Configuration;
using Hangfire;
using Hangfire.Server;
using Infrastructure.Clients.GrayLog;
using Infrastructure.Facades;
using Models;
using Models.Nuuk;
using Models.ServiceLayer.BusinessPartner;
using Models.ServiceLayer.Counties;
using Models.ServiceLayer.Documents;
using Models.Verweis;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Domain.SAP.BusinessPartner
{
    public class BusinessPartnerService : InjectionBase, IBusinessPartnerService
    {
        public BusinessPartnerService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            FlurlHttp.Configure(c =>
            {
                // Sets up the custom factory class in NewOrderModel to ignore certificate errors in Flurl requests
                c.CookiesEnabled = true;
                c.JsonSerializer = new NewtonsoftJsonSerializer(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            });

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        [Queue("business_partner")]
        [AutomaticRetry(OnAttemptsExceeded = AttemptsExceededAction.Delete, DelaysInSeconds = new[] { 60 })]
        public async Task UpsertBusinessPartner(string OrderNumber, int ecommerceID, PerformContext context)
        {
            int retryCount = context.GetJobParameter<int>("RetryCount");
            NewOrderModel orderModel = await BahngleisFacade.OrderRepo.GetOrderModelAsync(OrderNumber, ecommerceID);
            try
            {
                NewEcommerceModel ecommerceModel = await VerweisFacade.Repo.GetEcommerce(orderModel.EcommerceID);
                SAPDocuments documentsSAP = OrderFacade.SAPRepo.GetDocument(OrderNumber);

                if (!await OrderSpecification.OrderIsCanceled(ecommerceModel.Platform.Name, orderModel, documentsSAP, BahnMagentoFacade))
                {
                    BusinessPartnerModel businessPartner = await BusinessPartnerFacade.Repo.GetBusinessPartner(orderModel.Customer.TaxIdentification);
                    await Mapping(orderModel, businessPartner);

                    //------------------------------SEND TO SAP------------------------------
                    if (businessPartner.CardCode is null)
                        await BusinessPartnerFacade.Repo.AddBusinessPartner(businessPartner);
                    else
                        await BusinessPartnerFacade.Repo.UpdateBusinessPartner(businessPartner);

                    var cardCode = BusinessPartnerFacade.Repo.GetBusinessPartner(orderModel.Customer.TaxIdentification).Result.CardCode;
                    await EnqueueOrder(orderModel, cardCode);
                    await BahngleisFacade.OrderRepo.UpdateInDb(orderModel);
                }
                else
                {
                    orderModel.Status = await BahngleisFacade.OrderRepo.GetStatus("canceled");
                    await BahngleisFacade.OrderRepo.UpdateInDb(orderModel);
                }
            }
            catch (Exception exc)
            {
                GrayLogClient.GetLogger().Error(exc, "SAP - Error to update business partner", orderModel);
                await BahngleisFacade.OrderService.JobFailed(orderModel, exc, retryCount);

                throw exc;
            }
        }

        private async Task Mapping(NewOrderModel orderModel, BusinessPartnerModel businessPartner)
        {
            businessPartner.BPAddresses.Clear();
            businessPartner.BPFiscalTaxIDCollection.Clear();

            if (BusinessPartnerSpecification.IsCNPJ(orderModel.Customer.TaxIdentification))
                FillStateRegister(orderModel.Customer, orderModel.Payment.Address);
            FillFiscalData(businessPartner, orderModel.Customer);
            FillAddress(businessPartner, orderModel.Payment.Address);
            FillAddress(businessPartner, orderModel.Shipping.Address);
            await FillPayment(businessPartner, orderModel.Payment);

            BusinessPartnerHelper.CustomerTreatment(orderModel.Customer, businessPartner);

            businessPartner.EmailAddress = orderModel.Customer.Email;
            businessPartner.Password = orderModel.Customer.TaxIdentification;
            businessPartner.GroupCode = BusinessPartnerSpecification.IsTaxPayer(orderModel.Customer) ? BusinessPartnerHelper._taxPayerGroupCode : BusinessPartnerHelper._finalCustomerGroupCode;
            businessPartner.CardName = (!string.IsNullOrEmpty(orderModel.Customer.Reseller.CorporateName)) ? orderModel.Customer.Reseller.CorporateName : orderModel.Customer.Name;
            businessPartner.AliasName = orderModel.Customer.Alias;

            if (!string.IsNullOrEmpty(orderModel.Customer.PhoneNumber) && orderModel.Customer.PhoneNumber.Length >= 8)
            {
                businessPartner.Phone1 = orderModel.Customer.PhoneNumber.Length > 12 ? orderModel.Customer.PhoneNumber.Substring(2, 10) : orderModel.Customer.PhoneNumber.Substring(2, orderModel.Customer.PhoneNumber.Length - 2);
                businessPartner.Phone2 = orderModel.Customer.PhoneNumber.Substring(0, 2);
            }
        }

        //---------------------------------------------FILL LISTS---------------------------------------------
        public void FillStateRegister(NewCustomerModel customer, NewAddressModel billingAddress)
        {
            NuukCnpjModel taxITResponse = NuukFacade.Service.FindRegistry(billingAddress.UF, BusinessPartnerHelper.Unformat(customer.TaxIdentification)).Result;

            if (NuukSpecification.Found(taxITResponse))
            {
                var consultaCadastro = taxITResponse.Consulta;

                customer.Reseller.CorporateName = consultaCadastro.RazaoSocial ?? customer.Reseller.CorporateName;
                customer.Reseller.StateRegistry = (string.IsNullOrEmpty(consultaCadastro.InscricaoEstadual) || consultaCadastro.SituacaoIE.ToLower() == "inativo") ? "Isento" : consultaCadastro.InscricaoEstadual.Trim().ToLower();
            }
            else
                customer.Reseller.StateRegistry = "Isento";
        }

        public void FillFiscalData(BusinessPartnerModel businessPartner, NewCustomerModel customer)
        {
            if (BusinessPartnerSpecification.IsCNPJ(customer.TaxIdentification))
            {
                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "",
                    BPCode = businessPartner.CardCode,
                    TaxId0 = customer.TaxIdentification,
                    TaxId1 = string.IsNullOrEmpty(customer.Reseller.StateRegistry) ? null : customer.Reseller.StateRegistry,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "ENTREGA",
                    BPCode = businessPartner.CardCode,
                    TaxId0 = customer.TaxIdentification,
                    TaxId1 = string.IsNullOrEmpty(customer.Reseller.StateRegistry) ? null : customer.Reseller.StateRegistry,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "PAGAMENTO",
                    BPCode = businessPartner.CardCode,
                    TaxId0 = customer.TaxIdentification,
                    TaxId1 = string.IsNullOrEmpty(customer.Reseller.StateRegistry) ? null : customer.Reseller.StateRegistry,
                    AddrType = "bo_BillTo"
                });
            }
            else
            {
                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "",
                    BPCode = businessPartner.CardCode,
                    TaxId4 = customer.TaxIdentification,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "ENTREGA",
                    BPCode = businessPartner.CardCode,
                    TaxId4 = customer.TaxIdentification,
                    AddrType = "bo_ShipTo"
                });

                businessPartner.BPFiscalTaxIDCollection.Add(new BusinessPartnerFiscalTaxIdCollection
                {
                    Address = "PAGAMENTO",
                    BPCode = businessPartner.CardCode,
                    TaxId4 = customer.TaxIdentification,
                    AddrType = "bo_BillTo"
                });
            }
        }

        public void FillAddress(BusinessPartnerModel businessPartner, NewAddressModel address)
        {
            var ieIndicator = BusinessPartnerSpecification.IsTaxPayer(businessPartner) ? BusinessPartnerHelper._taxPayerIndicator : BusinessPartnerHelper._notTaxPayerIndicator;

            var county = GetCounty(address);
            businessPartner.BPAddresses.Add(BusinessPartnerHelper.ParseAddress(address, businessPartner.CardCode, ieIndicator, county));
        }

        public async Task FillPayment(BusinessPartnerModel businessPartner, NewPaymentModel payment)
        {
            MappedMethod ecommerceMethod = await VerweisFacade.Repo.GetMethod(payment.Method, null);

            if (ecommerceMethod != null)
            {
                if (BusinessPartnerSpecification.ShouldAddPaymentMethod(businessPartner.BPPaymentMethods, ecommerceMethod.ERPName))
                {
                    businessPartner.BPPaymentMethods.Add(new BusinessPartnerPaymentMethods
                    {
                        PaymentMethodCode = ecommerceMethod.ERPName
                    });
                }
                businessPartner.PeymentMethodCode = ecommerceMethod.ERPName;
            }
            else
                businessPartner.PeymentMethodCode = "";
        }


        public void Treatment(NewOrderModel NewOrderModel)
        {
            //------------------------------MODEL TREATMENT BAHNGLEIS------------------------------
            //AddressTreatment.Treatment(NewOrderModel.Customer.Address, "Customer");
            //AddressTreatment.Treatment(NewOrderModel.Shipping.Address, "Entrega");
            //AddressTreatment.Treatment(NewOrderModel.Payment.Address, "Pagamento");

            //CustomerTreatment.Treatment(NewOrderModel.Customer, NewOrderModel.Shipping.Address);

            //PaymentTreatment.Treatment(NewOrderModel.Payment, NewOrderModel.Channel, NewOrderModel.ChannelOrderID, NewOrderModel.Number);

            //ProductTreatment.Treatment(NewOrderModel.Products);

            //OrderTreatment.Treatment(NewOrderModel);
        }


        //---------------------------------------------AUXILIARIES---------------------------------------------
        public string GetCounty(NewAddressModel address)
        {
            //--------------------SAP--------------------
            CountyModel county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{address.City.Replace("'", "").ToUpper()}' and State eq '{address.UF.ToUpper()}'").Result.FirstOrDefault();

            if (county == null)
                county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(address.City).Replace("'", "").ToUpper()}' and State eq '{address.UF.ToUpper()}'").Result.FirstOrDefault();

            if (county == null && address.City.Contains("("))
                county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{address.City.Split('(', ')')[1].ToUpper()}' and State eq '{address.UF.ToUpper()}'").Result.FirstOrDefault();

            if (county == null && !string.IsNullOrEmpty(address.IbgeCode))
                county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=IbgeCode eq '{address.IbgeCode}'").Result.FirstOrDefault();

            //--------------------VIA CEP--------------------
            if (county == null)
            {
                var result = OrderFacade.ShippingRepo.GetAddressViaCEP(address.ZipCode);

                if (!string.IsNullOrEmpty(result.localidade))
                {
                    if (county == null && !string.IsNullOrEmpty(result.ibge))
                        county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=IbgeCode eq '{result.ibge}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{result.localidade.ToUpper()}' and State eq '{result.uf.ToUpper()}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(result.localidade).Replace("'", "").ToUpper()}' and State eq '{address.UF.ToUpper()}'").Result.FirstOrDefault();

                    if (county != null)
                    {
                        address.District = string.IsNullOrEmpty(result.bairro) ? address.District : result.bairro;
                        address.City = string.IsNullOrEmpty(result.localidade) ? address.City : result.localidade;
                        address.UF = string.IsNullOrEmpty(result.uf) ? address.UF : result.uf;
                    }
                }
            }
            //--------------------REPUBLICA VIRTUAL--------------------
            if (county == null)
            {
                var result = OrderFacade.ShippingRepo.GetAddressRepublicaVirtual(address.ZipCode);

                if (!string.IsNullOrEmpty(result.cidade))
                {
                    county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{result.cidade.ToUpper()}' and State eq '{result.uf.ToUpper()}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(result.cidade).Replace("'", "").ToUpper()}' and State eq '{address.UF.ToUpper()}'").Result.FirstOrDefault();

                    if (county != null)
                    {
                        address.District = string.IsNullOrEmpty(result.bairro) ? address.District : result.bairro;
                        address.City = string.IsNullOrEmpty(result.cidade) ? address.City : result.cidade;
                        address.UF = string.IsNullOrEmpty(result.uf) ? address.UF : result.uf;
                    }
                }
            }

            //--------------------CORREIOS--------------------
            if (county == null)
            {
                var result = OrderFacade.ShippingRepo.GetAddressCorreios(address.ZipCode);

                if (!string.IsNullOrEmpty(result.cidade))
                {
                    county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{result.cidade.ToUpper()}' and State eq '{result.uf.ToUpper()}'").Result.FirstOrDefault();

                    if (county == null)
                        county = ServiceLayer.HttpClient.GetAsync<CountyModel>($"?$filter=Name eq '{BusinessPartnerHelper.RemoveAccents(result.cidade).Replace("'", "").ToUpper()}' and State eq '{address.UF.ToUpper()}'").Result.FirstOrDefault();

                    if (county != null)
                    {
                        address.District = string.IsNullOrEmpty(result.bairro) ? address.District : result.bairro;
                        address.City = string.IsNullOrEmpty(result.cidade) ? address.City : result.cidade;
                        address.UF = string.IsNullOrEmpty(result.uf) ? address.UF : result.uf;
                    }
                }
            }

            if (county is null)
                throw new Exception($"O CEP {address.ZipCode} é inválido.");

            return county.AbsId.ToString();
        }

        private async Task EnqueueOrder(NewOrderModel orderModel, string entry)
        {
            string jobID = BackgroundJob.Enqueue(() => OrderFacade.SAPService.AddOrder(orderModel.Number, orderModel.EcommerceID, null));

            orderModel.Documents.Add(new NewDocumentsModel
            {
                DocumentType = await BahngleisFacade.OrderRepo.GetDocumentType("Pedido"),
                JobID = jobID,
                Entry = entry,
                JobSucceeded = false
            });
        }

    }
}
