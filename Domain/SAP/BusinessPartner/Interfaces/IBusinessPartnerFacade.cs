namespace Domain.SAP.BusinessPartner.Interfaces
{
    public interface IBusinessPartnerFacade
    {
        IBusinessPartnerService Service { get; }
        IBusinessPartnerRepository Repo { get; }
    }
}