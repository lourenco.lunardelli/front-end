﻿using Hangfire.Server;
using Models;
using Models.ServiceLayer.BusinessPartner;
using System.Threading.Tasks;

namespace Domain.SAP.BusinessPartner.Interfaces
{
    public interface IBusinessPartnerService
    {
        Task UpsertBusinessPartner(string OrderNumber, int ecommerceID, PerformContext context);

        void FillAddress(BusinessPartnerModel businessPartner, NewAddressModel address);
        void FillFiscalData(BusinessPartnerModel businessPartner, NewCustomerModel customer);
        Task FillPayment(BusinessPartnerModel businessPartner, NewPaymentModel payment);

        string GetCounty(NewAddressModel address);
    }
}