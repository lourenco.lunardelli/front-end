﻿using Models.ServiceLayer.BusinessPartner;
using System.Threading.Tasks;

namespace Domain.SAP.BusinessPartner.Interfaces
{
    public interface IBusinessPartnerRepository
    {
        Task<BusinessPartnerModel> AddBusinessPartner(BusinessPartnerModel businessPartner);
        Task<BusinessPartnerModel> GetBusinessPartner(string TaxVat);
        Task UpdateBusinessPartner(BusinessPartnerModel businessPartner);
    }
}