﻿using Domain.SAP.Order.Interfaces.Payment;
using Infrastructure.Facades;
using Models.ServiceLayer.Payments;
using System.Linq;

namespace Domain.SAP.Order.Payment
{
    public class PaymentRepository : IPaymentRepository
    {
        public PaymentTermsTypesModel GetPaymentTermsByGroupName(string groupName)
        {
            var filter = $"$filter=PaymentTermsGroupName eq '{groupName}'";
            var paymentTerm = ServiceLayer.HttpClient.GetAsync<PaymentTermsTypesModel>(filter).Result;

            return paymentTerm.FirstOrDefault();
        }

        public WizardPaymentMethodsModel GetWizardPaymentMethodsByPayMethCod(string paymentMethodCode)
        {
            var filter = $"$filter=PaymentMethodCode eq '{paymentMethodCode}'";
            var paymentTerm = ServiceLayer.HttpClient.GetAsync<WizardPaymentMethodsModel>(filter).Result;

            return paymentTerm.FirstOrDefault();
        }
    }
}
