﻿using Domain.SAP.Order.Interfaces.Shipping;
using Infrastructure.Clients.Correios;
using Infrastructure.Facades;
using Models.ServiceLayer.Shippings;
using Models.ViaCep;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Domain.SAP.Order.Shipping
{
    public class ShippingRepository : IShippingRepository
    {
        public string GetIncotermsByTrnspCode(int transportationCode)
        {
            var incotermsDic = new Dictionary<int, string>
            {
                { 1, "1" },     // PAC
                { 2, "1" },     // ESEDEX
                { 3, "1" },     // SEDEX
                { 4, "9" },     // RETIRADA NO BALCÃO
                { 5, "9" },     // MOTOBOY
                { 6, "1" },     // POR CONTA DO DESTINATÁRIO
                { 7, "0" },     // FRETE GRÁTIS
                { 8, "1" },     // FOB
                { 9, "1" },     // FOB - NÃO UTILIZAR
                { 10, "0" },    // CIF - BRASPRESS EXPRESSO
                { 11, "1" },    // FOB - SUL CARGO
                { 12, "0" },    // CIF - SUL CARGO
                { 13, "0" },    // CIF - JAMEF AÉREO
                { 14, "0" },    // CIF - JAMEF RODOVIÁRIO
                { 15, "1" },    // FOB - OTS
                { 16, "0" },    // CIF - OTS
                { 17, "0" },    // CIF - BAUER CARGAS
                { 18, "1" },    // FOB - BAUER CARGAS
                { 19, "1" },    // FOB - EXPRESSO JUNDIAI
                { 20, "0" },    // CIF - EXPRESSO JUNDIAI
                { 21, "1" },    // FOB - Princesa dos Campos
                { 22, "1" },    // FOB - ADG FERREIRA
                { 23, "1" },    // FOB - CARVALIMA TRANSPORTES LTDA
                { 24, "1" },    // FOB - EXPRESSO SÃO MIGUEL
                { 25, "0" },    // CIF - EXPRESSO SÃO MIGUEL
                { 26, "1" },    // FOB - RODONAVES
                { 27, "0" },    // CIF - TEX Transporte
                { 28, "1" },    // FOB - TEX Transporte
                { 29, "1" },    // FOB - MERIDIONAL CARGAS
                { 30, "0" },    // CIF - MERIDIONAL CARGAS
                { 31, "0" },    // CIF - SICALL CARGAS E ENCOMENDAS
                { 32, "1" },    // FOB  - SICALL CARGAS E ENCOMENDAS
                { 33, "1" },    // CORREIOS LOGÍSTICA REVERSA
                { 34, "0" },    // CIF - PLANALTO ENCOMENDAS
                { 35, "1" },    // FOB - PLANALTO ENCOMENDAS
                { 36, "1" },    // FOB - Aeropress
                { 37, "0" },    // CIF - Aeropress
                { 38, "1" },    // FOB - Pássaro Verde Express
                { 39, "0" },    // CIF - Pássaro Verde Express
                { 40, "0" },    // CIF - VRG LINHAS AÉREAS
                { 41, "1" },    // FOB - VRG LINHAS AÉREAS
                { 42, "0" },    // CIF - REUNIDAS TRANSPORTES
                { 43, "1" },    // FOB - REUNIDAS TRANSPORTES
                { 44, "0" },    // CIF - JADLOG (RODOVIARIO)
                { 45, "1" },    // FOB - JADLOG AÉREO
                { 46, "0" },    // CIF - OURO NEGRO
                { 47, "1" },    // FOB - OURO NEGRO
                { 48, "0" },    // CIF - JADLOG (AEREO)
                { 49, "0" },    // CIF - LOCAL EXPRESS
                { 50, "1" },    // FOB - LOCAL EXPRESS
                { 51, "0" },    // CIF - Rodonaves
                { 52, "1" },    // FOB - PEXLOG
                { 53, "0" },    // CIF - PEXLOG
                { 54, "1" },    // FOB - EUCATUR
                { 55, "0" },    // CIF - EUCATUR
                { 56, "0" },    // CIF - TW Transportes
                { 57, "1" },    // FOB - TW Transportes
                { 58, "1" },    // NÃO UTILIZAR
                { 59, "0" },    // CIF - TRANSPORTADORA GOBOR
                { 60, "1" },    // FOB - TRANSPORTADORA GOBOR
                { 61, "0" },    // CIF  - FedEx
                { 62, "1" },    // FOB - FedEx
                { 63, "1" },    // FOB - JAMEF AÉREO
                { 64, "1" },    // FOB - JAMEF RODOVIÁRIO
                { 65, "1" },    // FOB - Daytona Express
                { 66, "0" },    // CIF - Daytona Express
                { 67, "0" },    // CIF - BRASPRESS RODOVIÁRIO
                { 68, "1" },    // FOB - Braspress Rodoviário
                { 69, "1" },    // FOB - Braspress Expresso
                { 70, "0" },    // CIF - TNT (AEREO)
                { 71, "0" },    // CIF - TNT (RODOVIARIO)
                { 72, "0" },    // CIF - PLIMOR (RODOVIÁRIO)
                { 73, "0" },    // CIF - TOTAL EXPRESS (EXPRESSO)
                { 74, "1" },    // ACTION LOG
                { 75, "1" },    // FOB - METAR
                { 81, "1" }     // FOB - DIRECT EXPRESS
            };

            if (incotermsDic.TryGetValue(transportationCode, out string incoterms))
            {
                return incoterms;
            }

            throw new System.Exception("Incoterms não definido para o tipo de envio.");
        }

        public enderecoERP GetAddressCorreios(string zipCode)
        {
            try
            {
                AtendeClienteClient wsCorreios = new AtendeClienteClient();
                consultaCEPResponse response = wsCorreios.consultaCEPAsync(zipCode).Result;

                var address = new enderecoERP
                {
                    bairro = response.@return.bairro,
                    cep = response.@return.cep,
                    cidade = response.@return.cidade,
                    complemento2 = response.@return.complemento2,
                    end = response.@return.end,
                    uf = response.@return.uf,
                    unidadesPostagem = response.@return.unidadesPostagem
                };

                return address;

            }
            catch
            {
                return new enderecoERP();
            }
        }

        public ViaCepModel GetAddressViaCEP(string zipCode)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync($"{Environment.GetEnvironmentVariable("VIA_CEP_URL")}{zipCode}/json/").Result;
                    return JsonConvert.DeserializeObject<ViaCepModel>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch
            {
                return new ViaCepModel();
            }
        }

        public enderecoERP GetAddressRepublicaVirtual(string zipCode)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var response = client.GetAsync($"{Environment.GetEnvironmentVariable("REP_VIRTUAL_URL")}{zipCode}&formato=json").Result;
                    return JsonConvert.DeserializeObject<enderecoERP>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch
            {
                return new enderecoERP();
            }
        }

        public ShippingTypesModel GetShippingCode(string methodName)
        {
            var filter = $"$filter=Name eq '{methodName}'";

            return ServiceLayer.HttpClient.GetByQueryAsync<ShippingTypesModel>(filter, null).Result;
        }
    }
}
