using Domain.SAP.Order.Interfaces;
using Infrastructure.Facades;
using Models.ServiceLayer.Documents;
using Models.ServiceLayer.Item;
using Models.ServiceLayer.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.SAP.Order
{
    public class OrderRepository : IOrderRepository
    {
        public SAPOrderModel Get(int orderEntry)
        {
            return ServiceLayer.HttpClient.GetByIdAsync<SAPOrderModel>(orderEntry).Result;
        }

        public async Task<SAPOrderModel> Add(SAPOrderModel order)
        {
            return await ServiceLayer.HttpClient.PostAsync(order);
        }

        public ItemModel GetItem(string itemCode)
        {
            var filter = $"$filter=ItemCode eq '{itemCode.ToUpper()}'";
            var headers = new Dictionary<string, string>
            {
                { "B1S_CaseInsensitive", "true" }
            };

            var result = ServiceLayer.HttpClient.GetAsync<ItemModel>(filter, null, headers).Result.FirstOrDefault();

            if (result != null)
            {
                return result;
            }
            else
            {
                filter = $"$filter=ItemCode eq '{itemCode.ToLower()}'";
                result = ServiceLayer.HttpClient.GetAsync<ItemModel>(filter, null, headers).Result.FirstOrDefault();
            }

            if (result != null)
                return result;

            throw new Exception($"SKU {itemCode.ToUpper()} não encontrada no SAP.");
        }

        public SAPDocuments GetDocument(string number)
        {
            return ServiceLayer.HttpClient.GetByQueryAsync<SAPDocuments>("Orders", $"$filter=U_SHL_IDPedido eq { number }&$select=DocEntry,DocNum,Cancelled").Result;
        }
    }
}