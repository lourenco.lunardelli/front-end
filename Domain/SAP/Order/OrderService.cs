using Domain.SAP.BusinessPartner;
using Domain.SAP.Order.Interfaces;
using Hangfire;
using Hangfire.Server;
using Infrastructure.Clients.GrayLog;
using Infrastructure.Facades;
using Models;
using Models.ServiceLayer.Documents;
using Models.ServiceLayer.Orders;
using Models.ServiceLayer.Shippings;
using Models.Verweis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.SAP.Order
{
    public class OrderService : InjectionBase, IOrderService
    {
        public OrderService(IServiceProvider serviceProvider) : base(serviceProvider) { }

        [Queue("order")]
        [AutomaticRetry(OnAttemptsExceeded = AttemptsExceededAction.Delete, DelaysInSeconds = new[] { 60 })]
        public async Task AddOrder(string OrderNumber, int ecommerceID, PerformContext context)
        {
            NewOrderModel orderModel = await BahngleisFacade.OrderRepo.GetOrderModelAsync(OrderNumber, ecommerceID);
            orderModel.Documents.OrderByDescending(s => s.DocumentID).Last().JobSucceeded = true;
            int retryCount = context.GetJobParameter<int>("RetryCount");

            try
            {
                NewEcommerceModel ecommerceModel = await VerweisFacade.Repo.GetEcommerce(orderModel.EcommerceID);
                SAPDocuments documentsSAP = OrderFacade.SAPRepo.GetDocument(OrderNumber);

                if (!await OrderSpecification.OrderIsCanceled(ecommerceModel.Platform.Name, orderModel, documentsSAP, BahnMagentoFacade))
                {
                    if (OrderSpecification.OrderNotExists(documentsSAP))
                    {
                        SAPOrderModel orderSAP = new SAPOrderModel
                        {
                            TaxExtension = new TaxExtensionModel(),
                            DocumentLines = new List<DocumentLineModel>(),
                            DocumentAdditionalExpenses = new List<AdditionalExpenseModel>(),
                            U_SHL_IDPedido = orderModel.Number,
                            BPL_IDAssignedToInvoice = OrderSpecification.GetBPLID(),
                            SalesPersonCode = orderModel.CustomerEcommerceID,
                            Comments = DateTime.Now + " CS "
                        };

                        await FillTaxInfo(orderModel, orderSAP);

                        FillProducts(orderModel, orderSAP);

                        FillShippingMethod(orderModel, orderSAP);

                        FillAdditionalExpense(orderModel, orderSAP);

                        FillPaymentInfo(orderModel, orderSAP);

                        FillSkillPayment(orderSAP, orderSAP.PaymentMethod);

                        var docEntry = OrderFacade.SAPRepo.Add(orderSAP).Result.DocEntry.ToString();
                        await EnqueuePicking(orderModel, docEntry);
                        await BahngleisFacade.OrderRepo.UpdateInDb(orderModel);
                    }
                }
                else
                {
                    orderModel.Status = await BahngleisFacade.OrderRepo.GetStatus("canceled");
                    await BahngleisFacade.OrderRepo.UpdateInDb(orderModel);
                }
            }
            catch (Exception exc)
            {
                GrayLogClient.GetLogger().Error(exc, "SAP - Error adding OrderModel", orderModel);
                await BahngleisFacade.OrderService.JobFailed(orderModel, exc, retryCount);

                throw exc;
            }
        }

        //---------------------------------------------FILL LISTS---------------------------------------------
        public async Task FillTaxInfo(NewOrderModel orderModel, SAPOrderModel orderSAP)
        {
            var businessPartner = await BusinessPartnerFacade.Repo.GetBusinessPartner(orderModel.Customer.TaxIdentification);
            orderSAP.CardCode = businessPartner.CardCode;

            if (orderModel.Shipping.IsDropShipping)
                orderSAP.TaxExtension.MainUsage = BusinessPartnerHelper._sellToDemand;
            else if (BusinessPartnerSpecification.IsTaxPayer(businessPartner))
                orderSAP.TaxExtension.MainUsage = BusinessPartnerHelper._resale;
            else
                orderSAP.TaxExtension.MainUsage = BusinessPartnerHelper._finalConsumer;
        }

        public void FillProducts(NewOrderModel orderModel, SAPOrderModel orderSAP)
        {
            foreach (var product in orderModel.Products)
            {
                DocumentLineModel documentLine = new DocumentLineModel
                {
                    ItemCode = OrderFacade.SAPRepo.GetItem(product.SKU).ItemCode,
                    Usage = orderSAP.TaxExtension.MainUsage.ToString(),
                    Quantity = product.Quantity,
                    ItemDescription = product.Name.Length > OrderSpecification._sapMaxItemLength ? product.Name.Substring(0, OrderSpecification._sapMaxItemLength) : product.Name
                };

                CalculatePrice(documentLine, product);

                orderSAP.DocumentLines.Add(documentLine);
            }
        }

        public void FillShippingMethod(NewOrderModel orderModel, SAPOrderModel orderSAP)
        {
            var shippingCode = OrderFacade.ShippingRepo.GetShippingCode(orderModel.Shipping.Method);

            if (shippingCode != null)
            {
                orderSAP.TransportationCode = shippingCode.Code;

                //TODO: tornar campo configurável
                switch (orderModel.Shipping.Method.ToUpper())
                {
                    case "JADLOG RODOVIÁRIO":
                    case "JADLOG EXPRESSO":
                        orderSAP.Comments = orderModel.Shipping.Method;
                        //orderSAP.TaxExtension.PackQuantity = orderModel.Shipping.PackagesCount;
                        orderSAP.TaxExtension.Carrier = "F00204";
                        break;


                    case "correios_21":
                        orderSAP.Comments = "Total Express (expresso)";
                        //orderSAP.TaxExtension.PackQuantity = orderModel.Shipping.PackagesCount;
                        orderSAP.TaxExtension.Carrier = "F00367";
                        break;


                    case "TNT (RODOVIÁRIO)":
                        orderSAP.Comments = orderModel.Shipping.Method;
                        //orderSAP.TaxExtension.PackQuantity = orderModel.Shipping.PackagesCount;
                        orderSAP.TaxExtension.Carrier = "F00371";
                        break;

                    case "JAMEF AÉREO":
                    case "JAMEF RODOVIÁRIO":
                        orderSAP.Comments = orderModel.Shipping.Method;
                        //orderSAP.TaxExtension.PackQuantity = orderModel.Shipping.PackagesCount;
                        orderSAP.TaxExtension.Carrier = "F00205";
                        break;

                    case "BRASPRESS EXPRESSO":
                    case "BRASPRESS RODOVIÁRIO":
                        orderSAP.Comments = orderModel.Shipping.Method;
                        //orderSAP.TaxExtension.PackQuantity = orderModel.Shipping.PackagesCount;
                        orderSAP.TaxExtension.Carrier = "F00054";
                        break;
                    default:
                        break;
                }

                //BSD-13488
                orderSAP.DocDueDate = orderModel.Shipping.DueDate != null ? Convert.ToDateTime(orderModel.Shipping.DueDate) : DateTime.Today;

                orderSAP.ClosingRemarks = (orderModel.Shipping.Address.Complement != null && orderModel.Shipping.Address.Complement.Length > 30) ? orderModel.Shipping.Address.Complement : "";
            }
            else
                throw new Exception($"Método de envio [{orderModel.Shipping.Method}] não encontrado no ERP");
        }

        public void FillAdditionalExpense(NewOrderModel orderModel, SAPOrderModel orderSAP)
        {
            // MONE-368 B2W - Retirar frete das Notas Fiscais de saída
            ChannelModel channel = new ChannelModel();
            Channels.channels.TryGetValue(orderModel.Channel.ToLower(), out channel);

            if (channel == null || (channel.HaveShipping && BusinessPartnerSpecification.IsCNPJ(orderModel.Customer.TaxIdentification)))
            {
                if (orderModel.Shipping.Price > 0)
                {
                    orderSAP.DocumentAdditionalExpenses.Add(new AdditionalExpenseModel
                    {
                        ExpenseCode = 1,
                        LineTotal = orderModel.Shipping.Price
                    });
                }
            }
        }

        public void FillPaymentInfo(NewOrderModel orderModel, SAPOrderModel orderSAP)
        {
            var paymentCode = OrderFacade.PaymentRepo.GetPaymentTermsByGroupName(orderModel.Shipping.Method);

            //TODO: verificar como vai ser feito com boleto, atenção para o tipo de boleto (BoletoFaturadoTerm)
            // A solução pode ser enviar os dados de BoletoFaturadoTerm no campo direto no payment method (mapeado na Verweis) Ex: 14, 21 e 28 dias

            //if (orderModel.Payment.BoletoTaxAmount > 0)
            //{
            //    orderSAP.DocumentAdditionalExpenses.Add(new AdditionalExpenseModel
            //    {
            //        ExpenseCode = 11,
            //        LineTotal = orderModel.Payment.BoletoTaxAmount
            //    });
            //}

            //if (orderModel.Payment.Method.Equals("bringit_boletofaturado"))
            //{
            //    // For boleto faturado we have only one payment match, since the payment term is specified by the customer in Magento.
            //    SAPPaymentModel = orderModel.Company.CompanyPymtMatchModelList.FirstOrDefault(x => x.EcommPymtReferenceName.Equals("bringit_boletofaturado"));

            //    if (SAPPaymentModel != null)
            //    {
            //        orderSAP.PaymentMethod = SAPPaymentModel.SAPPaymentType.PayMethodCode;
            //        orderSAP.PaymentGroupCode = OrderFacade.PaymentRepo.GetPaymentTermsByGroupName(orderModel.Payment.BoletoFaturadoTerm).GroupNumber;
            //    }
            //}
            if (paymentCode != null)
            {
                //TODO:verificar se é necessário enviar esse campo

                //orderSAP.PaymentMethod = SAPPaymentModel.SAPPaymentType.PayMethodCode;
                orderSAP.PaymentGroupCode = paymentCode.GroupNumber;
            }
            else
                throw new Exception($"Método de pagamento [{orderModel.Payment.Method}] não encontrado no ERP");

        }

        public void FillSkillPayment(SAPOrderModel orderSAP, string paymMethod)
        {
            // BSD-9542 Sets the Skill user field for the payment method.

            //TODO: irá virar um campo configurável
            if (string.IsNullOrEmpty(paymMethod))
            {
                orderSAP.U_SKILL_FormaPagto = "99";
                return;
            }

            var wizardPayment = OrderFacade.PaymentRepo.GetWizardPaymentMethodsByPayMethCod(paymMethod);

            switch (wizardPayment.PaymentMeans)
            {
                case "bopmBillOfExchange":
                    orderSAP.U_SKILL_FormaPagto = "15";
                    break;
                case "bopmBankTransfer":
                    orderSAP.U_SKILL_FormaPagto = "99";
                    break;
            }
        }


        private async Task EnqueuePicking(NewOrderModel orderModel, string entry)
        {
            string jobID = BackgroundJob.Enqueue(() => PickingFacade.Service.GeneratePickList(orderModel.Number, orderModel.EcommerceID));

            orderModel.Documents.Add(new NewDocumentsModel
            {
                DocumentType = await BahngleisFacade.OrderRepo.GetDocumentType("Lista de Separação"),
                JobID = jobID,
                Entry = entry,
                JobSucceeded = false
            });
        }

        public void CalculatePrice(DocumentLineModel documentLine, NewProductModel product)
        {
            double unitDiscount = 0;

            if (product.Discount > 0)
                unitDiscount = product.Discount / product.Quantity;

            documentLine.UnitPrice = (product.Price - unitDiscount);
        }

        //---------------------------------------------RECURRING JOBS---------------------------------------------
        public async Task ScheduleMissingDocuments()
        {
            List<NewOrderModel> orders = await BahngleisFacade.OrderRepo.GetOrdersWithoutDocuments();

            foreach (var order in orders)
            {
                try
                {
                    object queryParams = new
                    {
                        order.Number,
                        order.EcommerceID
                    };

                    await Bahnhof.HttpClient.PostByQueryAsync(queryParams, "CreateMissingDocuments");
                }
                catch (Exception exc)
                {
                    GrayLogClient.GetLogger().Error(exc, $"SAP - ScheduleMissingDocuments - Error to schedule order {order.Number}");
                    continue;
                }

            }
        }

        public async Task UpdateDocumentsID()
        {
            var queryParams = new
            {
                DataIni = DateTime.Now.AddDays(-3).Date
            };

            List<NewOrderModel> OrdersList = Infrastructure.Facades.Bahngleis.HttpClient.GetAsync<NewOrderModel>(queryParams, "order/GetOrdersListDashboard").Result;

            foreach (var order in OrdersList)
            {
                try
                {
                    var query = new
                    {
                        order.OrderID
                    };

                    var orderSQL = Infrastructure.Facades.Bahngleis.HttpClient.GetByQueryAsync<NewOrderModel>(query, "Order/GetOrderById").Result;

                    await BahngleisFacade.OrderService.UpdateMagento(orderSQL);
                    await BahngleisFacade.OrderRepo.UpdateInDb(orderSQL);
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

    }
}