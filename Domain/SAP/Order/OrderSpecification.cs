using Domain.BahnMagento.Interfaces;
using Models;
using Models.ServiceLayer.Documents;
using System.Threading.Tasks;

namespace Domain.SAP.Order
{
    public static class OrderSpecification
    {
        public const int _sapMaxItemLength = 100;

        public static int GetBPLID()
        {
            return 1;
        }

        public static bool OrderNotExists(SAPDocuments SAPDocuments)
        {
            return SAPDocuments.DocEntry == 0;
        }

        public static bool ShouldAddShipping(SAPDocuments SAPDocuments)
        {
            return SAPDocuments.DocEntry == 0;
        }

        public static async Task<bool> OrderIsCanceled(string platformName, NewOrderModel order, SAPDocuments SAPDocuments, IBahnMagentoFacade facade)
        {
            if (order.Status.Name == "canceled" || SAPDocuments.Cancelled == "tYES")
                return true;

            if (IsMagento(platformName))
                return await IsCanceledMagento(order.EcommerceID, order.Number, facade);

            return false;
        }

        public static bool IsMagento(string platformName)
        {
            return (platformName == "MAGENTO");
        }

        public static async Task<bool> IsCanceledMagento(int ecommerceID, string number, IBahnMagentoFacade facade)
        {
            var orderMagento = await facade.Repo.GetOrderByNumber(ecommerceID, number);

            return (orderMagento.state == "closed" || orderMagento.state == "canceled");
        }
    }
}