﻿using Hangfire.Server;
using Models;
using Models.ServiceLayer.Orders;
using System.Threading.Tasks;

namespace Domain.SAP.Order.Interfaces
{
    public interface IOrderService
    {
        Task AddOrder(string OrderNumber, int ecommerceID, PerformContext context);
        Task ScheduleMissingDocuments();
        Task UpdateDocumentsID();
    }
}