﻿using Models.ServiceLayer.Documents;
using Models.ServiceLayer.Item;
using Models.ServiceLayer.Orders;
using System.Threading.Tasks;

namespace Domain.SAP.Order.Interfaces
{
    public interface IOrderRepository
    {
        Task<SAPOrderModel> Add(SAPOrderModel order);
        SAPOrderModel Get(int orderEntry);

        ItemModel GetItem(string itemCode);

        SAPDocuments GetDocument(string number);
    }
}