using Models.ServiceLayer.Payments;

namespace Domain.SAP.Order.Interfaces.Payment
{
    public interface IPaymentRepository
    {
        PaymentTermsTypesModel GetPaymentTermsByGroupName(string groupName);
        WizardPaymentMethodsModel GetWizardPaymentMethodsByPayMethCod(string paymentMethodCode);
    }
}