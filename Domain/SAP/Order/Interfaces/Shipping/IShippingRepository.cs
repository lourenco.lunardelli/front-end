﻿using Infrastructure.Clients.Correios;
using Models.ServiceLayer.Shippings;
using Models.ViaCep;

namespace Domain.SAP.Order.Interfaces.Shipping
{
    public interface IShippingRepository
    {
        enderecoERP GetAddressCorreios(string zipCode);
        enderecoERP GetAddressRepublicaVirtual(string zipCode);
        ViaCepModel GetAddressViaCEP(string zipCode);

        string GetIncotermsByTrnspCode(int transportationCode);
        ShippingTypesModel GetShippingCode(string methodName);

    }
}