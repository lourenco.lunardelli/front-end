using Domain.SAP.Order.Interfaces.Payment;
using Domain.SAP.Order.Interfaces.Shipping;

namespace Domain.SAP.Order.Interfaces
{
    public interface IOrderFacade
    {
        //-----------------------------------------SAP-----------------------------------------
        IOrderService SAPService { get; }
        IOrderRepository SAPRepo { get; }

        IShippingRepository ShippingRepo { get; }

        IPaymentRepository PaymentRepo { get; }
    }
}