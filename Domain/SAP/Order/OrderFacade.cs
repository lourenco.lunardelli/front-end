using Domain.SAP.Order.Interfaces;
using Domain.SAP.Order.Interfaces.Payment;
using Domain.SAP.Order.Interfaces.Shipping;
using Domain.SAP.Order.Payment;
using Domain.SAP.Order.Shipping;
using System;

namespace Domain.SAP.Order
{
    public class OrderFacade : IOrderFacade
    {
        private OrderService _sapService;
        private OrderRepository _sapRepo;
        private PaymentRepository _paymentRepo;
        private ShippingRepository _shippingRepo;

        private readonly IServiceProvider _serviceProvider;

        public OrderFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        //-----------------------------------------SAP-----------------------------------------
        public IOrderService SAPService
        {
            get
            {
                if (_sapService == null)
                    _sapService = new OrderService(_serviceProvider);
                return _sapService;
            }
        }
        public IOrderRepository SAPRepo
        {
            get
            {
                if (_sapRepo == null)
                    _sapRepo = new OrderRepository();
                return _sapRepo;
            }
        }

        public IShippingRepository ShippingRepo
        {
            get
            {
                if (_shippingRepo == null)
                    _shippingRepo = new ShippingRepository();
                return _shippingRepo;
            }
        }

        public IPaymentRepository PaymentRepo
        {
            get
            {
                if (_paymentRepo == null)
                    _paymentRepo = new PaymentRepository();
                return _paymentRepo;
            }
        }
    }
}