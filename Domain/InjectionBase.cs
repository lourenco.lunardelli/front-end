﻿using Domain.Bahngleis.Interfaces;
using Domain.BahnMagento.Interfaces;
using Domain.Nuuk.Interfaces;
using Domain.SAP.BusinessPartner.Interfaces;
using Domain.SAP.Order.Interfaces;
using Domain.SAP.Picking.Interfaces;
using Domain.Verweis.Interfaces;
using System;

namespace Domain
{
    public class InjectionBase
    {
        public readonly IBusinessPartnerFacade BusinessPartnerFacade;
        public readonly IOrderFacade OrderFacade;
        public readonly IPickingFacade PickingFacade;
        //public readonly IInvoiceFacade InvoiceFacade;
        //public readonly IIncomingPaymentFacade IncomingPaymentFacade;

        public readonly IBahngleisFacade BahngleisFacade;
        public readonly IBahnMagentoFacade BahnMagentoFacade;
        public readonly INuukFacade NuukFacade;
        public readonly IVerweisFacade VerweisFacade;

        public InjectionBase(IServiceProvider serviceProvider)
        {
            BusinessPartnerFacade = (IBusinessPartnerFacade)serviceProvider.GetService(typeof(IBusinessPartnerFacade));
            OrderFacade = (IOrderFacade)serviceProvider.GetService(typeof(IOrderFacade));
            PickingFacade = (IPickingFacade)serviceProvider.GetService(typeof(IPickingFacade));
            //InvoiceFacade = (IInvoiceFacade)serviceProvider.GetService(typeof(IInvoiceFacade));
            //IncomingPaymentFacade = (IIncomingPaymentFacade)serviceProvider.GetService(typeof(IIncomingPaymentFacade));

            BahngleisFacade = (IBahngleisFacade)serviceProvider.GetService(typeof(IBahngleisFacade));
            BahnMagentoFacade = (IBahnMagentoFacade)serviceProvider.GetService(typeof(IBahnMagentoFacade));
            NuukFacade = (INuukFacade)serviceProvider.GetService(typeof(INuukFacade));
            VerweisFacade = (IVerweisFacade)serviceProvider.GetService(typeof(IVerweisFacade));
        }
    }
}
