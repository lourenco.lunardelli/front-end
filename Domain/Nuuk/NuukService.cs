﻿using Domain.Nuuk.Interfaces;
using Infrastructure.Clients.Nuuk;
using Models.Nuuk;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Domain.Nuuk
{
    public class NuukService : InjectionBase, INuukService
    {
        public NuukService(IServiceProvider serviceProvider) : base(serviceProvider) { }


        public async Task<NuukCnpjModel> FindRegistry(string uf, string cnpj)
        {
            var queryResult = await Infrastructure.Facades.Nuuk.HttpClient.PostAsyncReceiveString(new
            {
                Filtros = new
                {
                    Cnpj = cnpj,
                    Uf = uf
                }
            }, NuukResources.ConsultaCadastroUrl);

            return JsonConvert.DeserializeObject<NuukCnpjModel>(queryResult);
        }
    }
}