﻿using Domain.Nuuk.Interfaces;
using System;

namespace Domain.Nuuk
{
    public class NuukFacade : INuukFacade
    {
        private NuukService _service;

        private readonly IServiceProvider _serviceProvider;

        public NuukFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public INuukService Service
        {
            get
            {
                if (_service == null)
                    _service = new NuukService(_serviceProvider);
                return _service;
            }
        }
    }
}
