﻿namespace Domain.Nuuk.Interfaces
{
    public interface INuukFacade
    {
        INuukService Service { get; }
    }
}