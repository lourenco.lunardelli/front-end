﻿using Models.Nuuk;
using System.Threading.Tasks;

namespace Domain.Nuuk.Interfaces
{
    public interface INuukService
    {
        Task<NuukCnpjModel> FindRegistry(string uf, string cnpj);
    }
}