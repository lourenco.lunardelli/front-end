﻿using Models.Nuuk;

namespace Domain.Nuuk
{
    public static class NuukSpecification
    {
        internal static bool Found(NuukCnpjModel consultaCadastro)
        {
            return !string.IsNullOrEmpty(consultaCadastro.Consulta.RazaoSocial);
        }
    }
}
