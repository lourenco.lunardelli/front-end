﻿using Domain.BahnMagento.Interfaces;
using System;

namespace Domain.BahnMagento
{
    public class BahnMagentoFacade : IBahnMagentoFacade
    {
        private IBahnMagentoRepository _repo;

        public IBahnMagentoRepository Repo
        {
            get
            {
                if (_repo == null)
                    _repo = new BahnMagentoRepository();
                return _repo;
            }
        }
    }
}
