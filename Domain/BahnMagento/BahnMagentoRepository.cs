using Domain.Bahngleis.Interfaces;
using Domain.BahnMagento.Interfaces;
using Infrastructure.Clients.Magento.ServiceReference;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.BahnMagento
{
    public class BahnMagentoRepository : IBahnMagentoRepository
    {
        public async Task<NewOrderModel> CastOrder(int ecommerceID, string number)
        {
            object query = new
            {
                ecommerceID,
                incrementID = number
            };

            return await Infrastructure.Facades.BahnMagento.HttpClient.GetByQueryAsync<NewOrderModel>(query, "Order/CastOrder");
        }

        public async Task HoldOrder(int ecommerceID, string number, string comment, bool notify = true)
        {
            object query = new
            {
                ecommerceID,
                incrementID = number,
                comment,
                notify
            };

            await Infrastructure.Facades.BahnMagento.HttpClient.PostByQueryAsync(query, "Order/HoldOrder");
        }

        public async Task AddComment(int ecommerceID, string number, string status, string comment, bool notify = true)
        {
            object query = new
            {
                ecommerceID,
                incrementID = number,
                status,
                comment,
                notify
            };

            await Infrastructure.Facades.BahnMagento.HttpClient.PostByQueryAsync(query, "Order/AddComment");
        }

        public async Task<List<salesOrderListEntity>> GetOrdersByStatus(int ecommerceID, string status)
        {
            object query = new
            {
                ecommerceID,
                status
            };

            return await Infrastructure.Facades.BahnMagento.HttpClient.GetAsync<salesOrderListEntity>(query, "Order/GetOrdersByStatus");
        }

        public async Task<salesOrderEntity> GetOrderByNumber(int ecommerceID, string number)
        {
            object query = new
            {
                ecommerceID,
                incrementID = number
            };

            return await Infrastructure.Facades.BahnMagento.HttpClient.GetByQueryAsync<salesOrderEntity>(query, "Order/GetOrderByNumber");
        }
    }
}