﻿namespace Domain.BahnMagento.Interfaces
{
    public interface IBahnMagentoFacade
    {
        IBahnMagentoRepository Repo { get; }
    }
}