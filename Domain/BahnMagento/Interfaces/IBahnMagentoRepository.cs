﻿using Infrastructure.Clients.Magento.ServiceReference;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.BahnMagento.Interfaces
{
    public interface IBahnMagentoRepository
    {
        Task<NewOrderModel> CastOrder(int ecommerceID, string number);
        Task HoldOrder(int ecommerceID, string number, string comment, bool notify = true);
        Task AddComment(int ecommerceID, string number, string status, string comment, bool notify = true);

        Task<List<salesOrderListEntity>> GetOrdersByStatus(int ecommerceID, string status);
        Task<salesOrderEntity> GetOrderByNumber(int ecommerceID, string number);
    }
}