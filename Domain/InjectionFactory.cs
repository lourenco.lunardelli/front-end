using Domain.Bahngleis;
using Domain.Bahngleis.Interfaces;
using Domain.BahnMagento;
using Domain.BahnMagento.Interfaces;
using Domain.Nuuk;
using Domain.Nuuk.Interfaces;
using Domain.SAP.BusinessPartner;
using Domain.SAP.BusinessPartner.Interfaces;
using Domain.SAP.Order;
using Domain.SAP.Order.Interfaces;
using Domain.SAP.Picking;
using Domain.SAP.Picking.Interfaces;
using Domain.Verweis;
using Domain.Verweis.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Domain
{
    public class InjectionFactory
    {
        public InjectionFactory(IServiceCollection services)
        {
            services.AddTransient<IBusinessPartnerFacade, BusinessPartnerFacade>(); 
            services.AddTransient<IOrderFacade, OrderFacade>();
            services.AddTransient<IPickingFacade, PickingFacade>();

            //services.AddTransient<IInvoiceFacade, InvoiceFacade>();
            //services.AddTransient<IIncomingPaymentFacade, IncomingPaymentFacade>();

            services.AddTransient<IBahngleisFacade, BahngleisFacade>();
            services.AddTransient<IBahnMagentoFacade, BahnMagentoFacade>();
            services.AddTransient<INuukFacade, NuukFacade>();
            services.AddTransient<IVerweisFacade, VerweisFacade>();
        }
    }
}