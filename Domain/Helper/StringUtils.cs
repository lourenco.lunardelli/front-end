﻿namespace Domain.Helper
{
    public class StringUtils
    {
        private static readonly bool[] _lookup;

        public static string RemoveInvalidXmlCharacters(string value)
        {
            if (value == null)
                return null;

            return value
                .Replace(">", string.Empty)
                .Replace("<", string.Empty)
                .Replace("&", string.Empty)
                .Replace("\t", " ")//TAB
                .Replace("\u0009", " ")//TAB CHAR
                .Replace("\"", string.Empty)
                .Replace("’", string.Empty)
                .Replace("'", string.Empty);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            char[] buffer = new char[str.Length];
            int index = 0;
            foreach (char c in str)
            {
                if (_lookup[c])
                {
                    buffer[index] = c;
                    index++;
                }
            }
            return new string(buffer, 0, index);
        }


        public static string StringTretamentDefault(string value)
        {
            var trimmed = "";
            if (value != null)
                trimmed = value.Trim();

            return trimmed;
        }


    }
}
