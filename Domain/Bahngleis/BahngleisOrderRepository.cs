using Domain.Bahngleis.Interfaces;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Bahngleis
{
    public class BahngleisOrderRepository : IBahngleisOrderRepository
    {

        public async Task<List<NewOrderModel>> GetOrdersWithoutDocuments(string documentType = "invoice")
        {
            object query = new
            {
                documentType
            };

            return await Infrastructure.Facades.Bahngleis.HttpClient.GetAsync<NewOrderModel>(query, "Order/GetOrdersWithoutDocument");
        }

        public async Task<NewOrderModel> GetOrderModelAsync(string number, int ecommerceID)
        {
            object query = new
            {
                number,
                ecommerceID
            };

            return await Infrastructure.Facades.Bahngleis.HttpClient.GetByQueryAsync<NewOrderModel>(query, "Order/GetOrder");
        }

        public async Task<NewOrderModel> GetOrderModelAsync(string orderId)
        {
            return await Infrastructure.Facades.Bahngleis.HttpClient.GetByQueryAsync<NewOrderModel>(new { orderId },"Order/GetOrderById");
        }

        public async Task UpdateInDb(NewOrderModel order)
        {
            await Infrastructure.Facades.Bahngleis.HttpClient.PutAsync(order.OrderID, order, "Order/UpdateOrder");
        }

        public async Task<DocumentType> GetDocumentType(string name)
        {
            return await Infrastructure.Facades.Bahngleis.HttpClient.GetByQueryAsync<DocumentType>(new { name }, "DocumentType/GetByName");
        }

        public async Task<NewStatusModel> GetStatus(string name)
        {
            return await Infrastructure.Facades.Bahngleis.HttpClient.GetByQueryAsync<NewStatusModel>(new { name }, "Status/GetByName");
        }
    }
}