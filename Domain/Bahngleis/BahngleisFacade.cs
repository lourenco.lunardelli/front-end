﻿using Domain.Bahngleis.Interfaces;
using System;

namespace Domain.Bahngleis
{
    public class BahngleisFacade : IBahngleisFacade
    {
        private IBahngleisOrderService _orderService;
        private IBahngleisOrderRepository _orderRepo;

        private readonly IServiceProvider _serviceProvider;

        public BahngleisFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IBahngleisOrderService OrderService
        {
            get
            {
                if (_orderService == null)
                    _orderService = new BahngleisOrderService(_serviceProvider);
                return _orderService;
            }
        }

        public IBahngleisOrderRepository OrderRepo
        {
            get
            {
                if (_orderRepo == null)
                    _orderRepo = new BahngleisOrderRepository();
                return _orderRepo;
            }
        }
    }
}
