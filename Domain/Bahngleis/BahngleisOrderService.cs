﻿using Domain.Bahngleis.Interfaces;
using Domain.SAP.Order;
using Models;
using Models.ServiceLayer.Documents;
using Models.Verweis;
using System;
using System.Threading.Tasks;

namespace Domain.Bahngleis
{
    public class BahngleisOrderService : InjectionBase, IBahngleisOrderService
    {
        public BahngleisOrderService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public async Task JobFailed(NewOrderModel order, Exception exc, int retryCount = 0)
        {
            NewEcommerceModel ecommerceModel = await VerweisFacade.Repo.GetEcommerce(order.EcommerceID);

            if (ecommerceModel.Platform.Name.ToUpper().Equals("MAGENTO") && retryCount == 10)
            {
                order.Status = await BahngleisFacade.OrderRepo.GetStatus("holded");
                await BahnMagentoFacade.Repo.AddComment(order.EcommerceID, order.Number, "holded", "Erro na integração com SAP: " + exc.Message, false);
            }
            else
                order.Status = await BahngleisFacade.OrderRepo.GetStatus("error");

            await BahngleisFacade.OrderRepo.UpdateInDb(order);
        }

        public async Task UpdateMagento(NewOrderModel order)
        {
            NewEcommerceModel ecommerceModel = await VerweisFacade.Repo.GetEcommerce(order.EcommerceID);

            if (OrderSpecification.IsMagento(ecommerceModel.Platform.Name))
            {
                if (await OrderSpecification.IsCanceledMagento(order.EcommerceID, order.Number, BahnMagentoFacade))
                {
                    order.Status = await BahngleisFacade.OrderRepo.GetStatus("canceled");
                    SAPDocuments documentsSAP = OrderFacade.SAPRepo.GetDocument(order.Number);

                    object query = new
                    {
                        U_Status_WMS = "CANCELADO"
                    };

                    Infrastructure.Facades.ServiceLayer.HttpClient.PatchAsync(documentsSAP.DocEntry, query, "Orders").Wait();
                }
            }
        }
    }
}
