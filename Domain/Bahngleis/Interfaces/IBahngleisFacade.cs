﻿namespace Domain.Bahngleis.Interfaces
{
    public interface IBahngleisFacade
    {
        IBahngleisOrderService OrderService { get; }
        IBahngleisOrderRepository OrderRepo { get; }
    }
}