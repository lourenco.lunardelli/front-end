﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Bahngleis.Interfaces
{
    public interface IBahngleisOrderRepository
    {
        Task<NewOrderModel> GetOrderModelAsync(string orderNumber, int ecommerceID);
        Task<NewOrderModel> GetOrderModelAsync(string orderId);
        Task<List<NewOrderModel>> GetOrdersWithoutDocuments(string documentType = "invoice");
        Task UpdateInDb(NewOrderModel order);

        Task<DocumentType> GetDocumentType(string name);
        Task<NewStatusModel> GetStatus(string name);
    }
}