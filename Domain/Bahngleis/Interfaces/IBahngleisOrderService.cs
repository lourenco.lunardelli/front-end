﻿using Models;
using System;
using System.Threading.Tasks;

namespace Domain.Bahngleis.Interfaces
{
    public interface IBahngleisOrderService
    {
        Task JobFailed(NewOrderModel order, Exception exc, int retryCount = 0);
        Task UpdateMagento(NewOrderModel order);
    }
}