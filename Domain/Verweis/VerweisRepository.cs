using Domain.Verweis.Interfaces;
using Models.Verweis;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Verweis
{
    public class VerweisRepository : IVerweisRepository
    {
        //------------------------------GET------------------------------
        public async Task<NewEcommerceModel> GetEcommerce(int ecommerceID)
        {
            return await Infrastructure.Facades.Verweis.HttpClient.GetByIdAsync<NewEcommerceModel>(ecommerceID, "Ecommerce/Get");
        }
        public async Task<List<NewEcommerceModel>> GetEcommerces()
        {
            return await Infrastructure.Facades.Verweis.HttpClient.GetAsync<NewEcommerceModel>("Ecommerce/Get");
        }

        public async Task<MappedMethod> GetMethod(string MethodID, string path)
        {
            return await Infrastructure.Facades.Verweis.HttpClient.GetByIdAsync<MappedMethod>(MethodID, path);
        }
    }
}