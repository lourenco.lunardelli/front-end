﻿namespace Domain.Verweis.Interfaces
{
    public interface IVerweisFacade
    {
        IVerweisRepository Repo { get; }
    }
}