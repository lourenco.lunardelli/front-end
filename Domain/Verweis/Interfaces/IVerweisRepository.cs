﻿using Models.Verweis;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Verweis.Interfaces
{
    public interface IVerweisRepository
    {
        Task<NewEcommerceModel> GetEcommerce(int ecommerceID);
        Task<MappedMethod> GetMethod(string MethodID, string path);
        Task<List<NewEcommerceModel>> GetEcommerces();
    }
}