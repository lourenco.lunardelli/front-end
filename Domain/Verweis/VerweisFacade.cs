﻿using Domain.Verweis.Interfaces;
using System;

namespace Domain.Verweis
{
    public class VerweisFacade : IVerweisFacade
    {
        private VerweisRepository _repo;

        private readonly IServiceProvider _serviceProvider;

        public VerweisFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IVerweisRepository Repo
        {
            get
            {
                if (_repo == null)
                    _repo = new VerweisRepository();
                return _repo;
            }
        }
    }
}
