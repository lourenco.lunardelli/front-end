using Infrastructure.Clients;
using Infrastructure.Clients.ServiceLayer;

namespace Infrastructure.Facades
{
    public static class ServiceLayer
    {
        private static IClient _httpClient;
        public static IClient HttpClient
        {
            get
            {
                if (_httpClient is null)
                    _httpClient = new ServiceLayerClient();
                
                return _httpClient;
            }
        }

        /// <summary>
        /// This method must be used for unit tests only
        /// </summary>
        /// <param name="client">The mocked client for testing</param>
        public static void SetClient(IClient client)
        {
            _httpClient = client;
        }
    }
}