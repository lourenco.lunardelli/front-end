using Infrastructure.Clients;
using Infrastructure.Clients.Bahnhof;

namespace Infrastructure.Facades
{
    public class Bahnhof
    {
        private static IClient _httpClient;
        public static IClient HttpClient
        {
            get
            {
                if (_httpClient is null)
                    _httpClient = new BahnhofClient();
                
                return _httpClient;
            }
        }

        /// <summary>
        /// This method must be used for unit tests only
        /// </summary>
        /// <param name="client">The mocked client for testing</param>
        public static void SetClient(IClient client)
        {
            _httpClient = client;
        }
    }
}