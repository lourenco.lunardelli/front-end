using Models.Roter;

namespace Infrastructure.Clients.Roter
{
    public class RoterResources : VirtualResources
    {
        public RoterResources()
        {
           ResourcesUrl.Add(typeof(PickingModel), PickingList);
        }
                
       private const string PickingList = "orders";
    }
}