﻿using System;
using System.Threading.Tasks;
using Flurl.Http;

namespace Infrastructure.Clients.Bahngleis
{
    public class BahngleisClient : VirtualClient
    {
        public BahngleisClient() : base(Environment.GetEnvironmentVariable("BAHNGLEIS_ADDRESS"), new BahngleisResources())
        {
            _client.Configure(settings => settings.HttpClientFactory = new CustomHttpClientFactory());
        }

        public override async Task HandleBeforeCallAsync(HttpCall call) { }

        public override async Task HandleAfterCallAsync(HttpCall call) { }
    }
}
