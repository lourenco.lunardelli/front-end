﻿using Models.Verweis;

namespace Infrastructure.Clients.Verweis
{
    public class VerweisResources : VirtualResources
    {
        public VerweisResources()
        {
            ResourcesUrl.Add(typeof(NewEcommerceModel), Ecommerce);
        }

        private const string Ecommerce = "api/Ecommerce";
    }
}
