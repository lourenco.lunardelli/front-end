using Flurl.Http;
using Flurl.Http.Configuration;
using Infrastructure.Clients.GrayLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Clients
{
    public abstract class VirtualClient : IClient
    {
        public readonly IFlurlClient _client;
        public VirtualResources RESOURCES;

        public VirtualClient(string addressUrl, VirtualResources virtualResources)
        {
            if (_client == null)
            {
                _client = new FlurlClient(addressUrl).Configure(settings =>
                {
                    settings.OnErrorAsync = HandleErrorAsync;
                    settings.BeforeCallAsync = HandleBeforeCallAsync;
                    settings.AfterCallAsync = HandleAfterCallAsync;

                    var jsonSettings = new JsonSerializerSettings
                    {
                        Formatting = Formatting.Indented,
                        ContractResolver = new DefaultContractResolver(),
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        PreserveReferencesHandling = PreserveReferencesHandling.None,
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore,
                        ObjectCreationHandling = ObjectCreationHandling.Replace,
                        Error = HandleDeserializationError
                    };
                    settings.JsonSerializer = new NewtonsoftJsonSerializer(jsonSettings);
                    //settings.HttpClientFactory = new CustomHttpBase();

                    settings.CookiesEnabled = true;
                });
            }

            RESOURCES = virtualResources;
        }


        //----------------GET----------------
        /// <summary>
        /// If path = null gets tha path from the resource for type T
        /// </summary>
        /// <param name="path"></param>
        /// <param name="query"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual async Task<List<T>> GetAsync<T>(object query = null, string path = null, Dictionary<string, string> header = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return await _client.Request()
                .AppendPathSegments(path)
                .SetQueryParams(query)
                .WithHeaders(header)
                .GetJsonAsync<List<T>>()
                .ConfigureAwait(false);
        }

        public virtual async Task<T> GetByIdAsync<T>(object entityId, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            if (entityId is string)
                path += $"/'{entityId}'";
            else if (entityId is int)
                path += $"/{entityId}";


            return await _client.Request()
                .AppendPathSegments(path)
                .GetJsonAsync<T>()
                .ConfigureAwait(false);
        }

        public virtual async Task<T> GetByQueryAsync<T>(object query = null, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return await _client.Request()
                .AppendPathSegments(path)
                .SetQueryParams(query)
                .GetJsonAsync<T>()
                .ConfigureAwait(false);
        }


        //----------------POST----------------
        public virtual async Task<T> PostAsync<T>(T entity, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return await _client.Request()
                .AppendPathSegment(path)
                .PostJsonAsync(entity)
                .ReceiveJson<T>()
                .ConfigureAwait(false);
        }

        public async Task<T> PostByQueryAsync<T>(T entity, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return await _client.Request()
                .AppendPathSegment(path)
                .SetQueryParams(entity)
                .PostJsonAsync(null)
                .ReceiveJson<T>()
                .ConfigureAwait(false);
        }


        //----------------PUT----------------
        public async Task<T> PutAsync<T>(object entityId, T entity, string path = null, object query = null)
        {
            if (path is null)
                path = RESOURCES.GetResource<T>();

            path += $"/{entityId}";

            return await _client.Request()
                
                .AppendPathSegment(path)
                .SetQueryParams(query)
                .PutJsonAsync(entity).ReceiveJson<T>()
                .ConfigureAwait(false);
        }

        public virtual async Task<T> PatchAsync<T>(object entityId, T entity, string path = null)
        {
            if (path is null)
                path = RESOURCES.GetResource<T>();

            path += $"/{entityId}";

            return await _client.Request()
                .AppendPathSegment(path)
                .PutJsonAsync(entity).ReceiveJson<T>()
                .ConfigureAwait(false);
        }

        //--------------------------------CUSTOMIZED--------------------------------
        public async Task<byte[]> GetAsyncNotDeserialized(object query = null, string path = null)
        {
            return await _client.Request()
                 .AppendPathSegments(path)
                 .SetQueryParams(query)
                 .GetAsync().Result.Content.ReadAsByteArrayAsync();

        }

        public async Task<string> PostAsyncReceiveString<T>(T entity, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return await _client.Request()
                            .AppendPathSegment(path)
                            .PostJsonAsync(entity)
                            .ReceiveString()
                            .ConfigureAwait(false);
        }

        public async Task PostAsyncNotDeserialized<T>(T entity, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            await _client.Request()
                            .AppendPathSegment(path)
                            .PostJsonAsync(entity)
                            .ConfigureAwait(false);
        }


        //--------------------------------ERRORS/HANDLERS--------------------------------
        public async Task HandleErrorAsync(HttpCall call)
        {
            string response = "";
            if (call.Response != null)
            {
                if (call.Response.Content != null)
                    response = await call.Response.Content.ReadAsStringAsync();


                //Logger.GetLogger().Error(call.Exception,
                //    $"Client request failed: " +
                //    $"StatusCode: ({(string.IsNullOrEmpty(call.Response.ToString()) ? call.Response.StatusCode : System.Net.HttpStatusCode.BadRequest)}), " +
                //    $"RequestMsg: {(string.IsNullOrEmpty(call.Response.ToString()) ? call.Response.RequestMessage : new System.Net.Http.HttpRequestMessage())}, " +
                //    $"ResponseContent: {response}");

                if (call.Response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    if (response.Contains("No matching records found") || response.Contains("Resource not found"))
                    {
                        throw new Exception("No matching records");
                    }
                }


                if (string.IsNullOrEmpty(call.Response.Content.ReadAsStringAsync().Result))
                {
                    throw call.Exception;
                }
                else
                {
                    if (response.Contains("code"))
                    {
                        var parsed = JObject.Parse(response);

                        if (!string.IsNullOrEmpty((string)parsed["error"]["message"]["value"]))
                        {
                            throw new Exception((string)parsed["error"]["message"]["value"]);
                        }
                    }
                    throw new Exception(response);
                }
            }
            throw call.Exception;
        }

        public static void HandleDeserializationError(object sender, ErrorEventArgs errorArgs)
        {
            var currentError = errorArgs.ErrorContext.Error.Message;
            errorArgs.ErrorContext.Handled = true;
        }


        public abstract Task HandleBeforeCallAsync(HttpCall call);

        public abstract Task HandleAfterCallAsync(HttpCall call);

    }


}