﻿using Infrastructure.Clients.Magento.ServiceReference;
using Models.Verweis;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Infrastructure.Clients.Magento
{
    public class MageConnection
    {
        public PortTypeClient MageService { get; set; }
        public string Session { get; set; }
        private DateTime Expiry { get; set; }

        private NewEcommerceModel _ecommerce;

        public MageConnection(NewEcommerceModel ecommerce)
        {
            _ecommerce = ecommerce;
            GetService();
        }


        public bool HasExpired(DateTime expiry)
        {
            return (expiry < DateTime.Now);
        }

        private PortTypeClient GetService()
        {
            Binding binding = GetBinding();

            if (MageService is null)
            {
                MageService = new PortTypeClient(binding, new EndpointAddress(_ecommerce.Url));

                Session = MageService.loginAsync(_ecommerce.User, _ecommerce.Password).Result;
                Expiry = DateTime.Now.AddMinutes(30);
            }
            else if (HasExpired(Expiry))
            {
                //MageService = new PortTypeClient(PortTypeClient.EndpointConfiguration.Port, magentoModel.Platform.Url);
                //MageService.Endpoint.Address = endpoint;

                Session = MageService.loginAsync(_ecommerce.User, _ecommerce.Password).Result;
                Expiry = DateTime.Now.AddMinutes(30);
            }

            return MageService;
        }

        private Binding GetBinding()
        {
            Binding binding = new BasicHttpsBinding();

            if (_ecommerce.Url.StartsWith("https"))
            {
                binding = new BasicHttpsBinding()
                {
                    MaxBufferSize = int.MaxValue,
                    ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max,
                    MaxReceivedMessageSize = int.MaxValue,
                    AllowCookies = true,
                };
            }
            else
            {
                var transportSecurityBinding = new BasicHttpBinding()
                {
                    MaxBufferSize = int.MaxValue,
                    ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max,
                    MaxReceivedMessageSize = int.MaxValue,
                    AllowCookies = true
                };

                var customTransportSecurityBinding = new CustomBinding(transportSecurityBinding);

                var textBindingElement = new TextMessageEncodingBindingElement
                {
                    MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.None)
                };

                // Replace text element to have Soap12 message version
                customTransportSecurityBinding.Elements[0] = textBindingElement;

                binding = customTransportSecurityBinding;
            }

            return binding;
        }
    }
}