﻿using Newtonsoft.Json;

namespace Infrastructure.Clients.TokenKeeper
{
    public class Token
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("expires_in")]
        public long ExpiresIn { get; set; }
        [JsonIgnore]
        public long ExpireTime { get; set; }
    }
}
