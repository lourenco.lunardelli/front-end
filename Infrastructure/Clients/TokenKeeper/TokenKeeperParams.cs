﻿using Newtonsoft.Json;

namespace Infrastructure.Clients.TokenKeeper
{
    public class TokenKeeperParams
    {
        [JsonProperty("xApiKey")]
        public string ApiKey { get; set; }
        [JsonProperty("client_id")]
        public string ClientId { get; set; }
    }
}
