﻿using Flurl.Http;
using Infrastructure.Clients.GrayLog;
using Models.ServiceLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Infrastructure.Clients.ServiceLayer
{
    public class ServiceLayerClient : VirtualClient
    {
        private readonly string _slCompany;
        private bool _isLogin;
        private bool _logedIn;
        private string _routeID;
        private long tokenTimout;

        public ServiceLayerClient() : base(Environment.GetEnvironmentVariable("SERVICE_LAYER_URL"), new ServiceLayerResources())
        {
            _client.Configure(settings =>
            {
                settings.HttpClientFactory = new CustomHttpClientFactory();
                settings.CookiesEnabled = true;
            });
            System.Net.ServicePointManager.Expect100Continue = false;
            _slCompany = Environment.GetEnvironmentVariable("SAP_DB");
        }

        public override async Task HandleBeforeCallAsync(HttpCall call)
        {
            await TryRenewToken();
        }

        public override async Task HandleAfterCallAsync(HttpCall call) { }


        private bool IsSessionUp()
        {
            //return _logedIn && DateTimeHelper.GetUnixTime(DateTime.Now) < _TokenTimout;
            return _logedIn && DateTimeOffset.UtcNow.ToUnixTimeSeconds() < tokenTimout;
        }

        private async Task TryRenewToken()
        {
            if (!_isLogin && !IsSessionUp())
            {
                if (!IsSessionUp())
                {
                    var loginResponseMessage = await LoginAsync(_slCompany);
                    if (loginResponseMessage == null)
                        throw new Exception("Não foi possível realizar o login na Service Layer.");

                    var cookies = loginResponseMessage.Headers.First(h => h.Key == "Set-Cookie");
                    var loginResponse = JsonConvert.DeserializeObject<ServiceLayerLoginModel>(loginResponseMessage.Content.ReadAsStringAsync().Result);
                    try
                    {
                        _routeID = cookies.Value.First(v => v.Contains("ROUTEID")).Split(';').First(v => v.Contains("ROUTEID"));
                    }
                    catch
                    {
                        // Use last _routeId if login does not respond a new one
                    }
                    var basichAuthToken = loginResponse.SessionId;
                    var auhenticationCookie = $"B1SESSION={basichAuthToken}; {_routeID}";
                    tokenTimout = DateTimeOffset.UtcNow.AddMinutes(loginResponse.SessionTimeout).ToUnixTimeSeconds();
                    _logedIn = true;
                }
            }
        }


        /// <summary>
        /// Do the loggin on the service layer
        /// </summary>
        /// <param name="company">Database company name that we need to connect.</param>
        /// <returns>Http message</returns>
        private async Task<HttpResponseMessage> LoginAsync(string company)
        {
            try
            {
                _isLogin = true;

                var url = RESOURCES.GetResource<ServiceLayerLoginModel>();

                var loginResponse = await _client.Request().AppendPathSegment(url).PostJsonAsync(
                    new
                    {
                        CompanyDB = company,

                        UserName = Environment.GetEnvironmentVariable("SAP_USERNAME"),
                        Password = Environment.GetEnvironmentVariable("SAP_PASSWORD")
                    });
                _isLogin = false;
                return loginResponse;
            }
            catch (Exception ex)
            {
                _isLogin = false;
                GrayLogClient.GetLogger().Error(ex, "Service layer login failed");
                return null;
            }
        }

        //--------------------------------SERVICE LAYER--------------------------------
        public override async Task<List<T>> GetAsync<T>(object query = null, string path = null, Dictionary<string, string> header = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();


            return (await _client.Request()
                .AppendPathSegments(path)
                .SetQueryParams(query)
                .WithHeaders(header)
                .GetJsonAsync<SLRoot<T>>()
                .ConfigureAwait(false)).Values;
        }

        public override async Task<T> GetByIdAsync<T>(object entityId, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            if (entityId is string)
                path += $"('{entityId}')";
            else if (entityId is int)
                path += $"({entityId})";

            return await _client.Request()
                .AppendPathSegments(path)
                .GetJsonAsync<T>()
                .ConfigureAwait(false);
        }


        public override async Task<T> GetByQueryAsync<T>(object query = null, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return (await _client.Request()
                .AppendPathSegments(path)
                .SetQueryParams(query)
                .GetJsonAsync<SLRoot<T>>()
                .ConfigureAwait(false)).Values.FirstOrDefault();
        }

        public override async Task<T> PostAsync<T>(T entity, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            return await _client.Request()
                .AppendPathSegment(path)
                .PostJsonAsync(entity)
                .ReceiveJson<T>()
                .ConfigureAwait(false);
        }

        public override async Task<T> PatchAsync<T>(object entityId, T entity, string path = null)
        {
            if (path == null)
                path = RESOURCES.GetResource<T>();

            if (entityId is string)
                path += $"('{entityId}')";
            else if (entityId is int)
                path += $"({entityId})";

            return await _client.Request()
                .AppendPathSegment(path)
                .WithHeader("B1S-ReplaceCollectionsOnPatch", "true")
                .PatchJsonAsync(entity)
                .ReceiveJson<T>()
                .ConfigureAwait(false);
        }
    }
}