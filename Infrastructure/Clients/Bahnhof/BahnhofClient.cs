﻿using System;
using System.Threading.Tasks;
using Flurl.Http;

namespace Infrastructure.Clients.Bahnhof
{
    public class BahnhofClient : VirtualClient
    {
        public BahnhofClient() : base(Environment.GetEnvironmentVariable("BAHNHOF_ADDRESS"), new BahnhofResources()) { }

        public override async Task HandleBeforeCallAsync(HttpCall call) { }


        public override async Task HandleAfterCallAsync(HttpCall call) { }
    }
}
