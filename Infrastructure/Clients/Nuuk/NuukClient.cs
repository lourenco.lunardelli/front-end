﻿using Flurl.Http;
using Infrastructure.Clients.TokenKeeper;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Clients.Nuuk
{
    public class NuukClient : VirtualClient
    {
        private TokenKeeperClient _tokenKeeper;
        
        public NuukClient() : base(Environment.GetEnvironmentVariable("NUUK_ADDRESS"), new NuukResources())
        {
            var tkParams = new TokenKeeperParams
            {
                ClientId = Environment.GetEnvironmentVariable("TK_NUUK_ID"),
                ApiKey = Environment.GetEnvironmentVariable("TK_NUUK_KEY")
            };
            _tokenKeeper = new TokenKeeperClient(tkParams);
        }

        public override async Task HandleBeforeCallAsync(HttpCall call)
        {
            var bearerToken = await _tokenKeeper.GetBearerToken();
            call.FlurlRequest.WithOAuthBearerToken(bearerToken);
        }

        public override async Task HandleAfterCallAsync(HttpCall call)
        {
        }
    }
}
