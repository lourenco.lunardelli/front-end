﻿using System;
using System.Threading.Tasks;
using Flurl.Http;

namespace Infrastructure.Clients.BahnMagento
{
    public class BahnMagentoClient : VirtualClient
    {
        public BahnMagentoClient() : base(Environment.GetEnvironmentVariable("BahnMagento_ADDRESS"), new BahnMagentoResources())
        {
            _client.Configure(settings => settings.HttpClientFactory = new CustomHttpClientFactory());
        }

        public override async Task HandleBeforeCallAsync(HttpCall call) { }

        public override async Task HandleAfterCallAsync(HttpCall call) { }
    }
}
