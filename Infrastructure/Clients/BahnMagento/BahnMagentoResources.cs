using Models;

namespace Infrastructure.Clients.BahnMagento
{
    public class BahnMagentoResources : VirtualResources
    {
        public BahnMagentoResources()
        {
            ResourcesUrl.Add(typeof(NewOrderModel), OrderUrl);
        }
        
        private const string OrderUrl = "Order";
        
    }
}