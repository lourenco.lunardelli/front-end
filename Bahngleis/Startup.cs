﻿using App.Metrics;
using App.Metrics.Health;
using Bahngleis.Models;
using Bahngleis.Models.DataManager;
using Bahngleis.Models.Repository;
using Bahngleis.Services;
using Domain;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Bahngleis
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IServiceProvider Service { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void Configure(IApplicationBuilder app)
        {
            app.UseCors("AllowAll");

            app.UseMetricsAllMiddleware();
            app.UseMetricsAllEndpoints();
            app.UseHealthAllEndpoints();

            app.UseMvc(routes =>
            {
                routes
                    .MapRoute(name: "api", template: "{controller}/{action}/{id?}");
            });

            // Ativando middlewares para uso do Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                // For Debug in Kestrel
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Bahngleis API");

            });


            UpdateDatabase(app);



        }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureHangfire(services);
            ConfigureCors(services);
            ConfigureContext(services);
            ConfigureMetrics(services);
            ConfigureDpInjection(services);
            ConfigureSwagger(services);

            Service = services.BuildServiceProvider();


            OrderService service = new OrderService(null, Service);
            service.ScheduleOrder();
        }

        public void ConfigureHangfire(IServiceCollection services)
        {
            Console.WriteLine(Environment.GetEnvironmentVariable("DB_CONNSTRING"));
            //-----------HANGFIRE-----------
            services.AddHangfire(configuration => configuration
                   .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                   .UseSimpleAssemblyNameTypeSerializer()
                   .UseRecommendedSerializerSettings()
                   .UsePostgreSqlStorage(Environment.GetEnvironmentVariable("DB_CONNSTRING"), new PostgreSqlStorageOptions
                   {
                       SchemaName = "hangfire_bahngleis"
                   })
                   );

            GlobalJobFilters.Filters.Add(new LogHangfire());

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute
            {
                DelaysInSeconds = new[] { 5, 30, 40, 50, 60, 100, 200, 600 },
                Attempts = 10,
                //OnAttemptsExceeded = AttemptsExceededAction.Delete
            });

        }

        public void ConfigureContext(IServiceCollection services)
        {
            Console.WriteLine(Environment.GetEnvironmentVariable("DB_CONNSTRING"));
            services.AddDbContext<ContextModels>(opts => opts
                                                             .UseLazyLoadingProxies()
                                                             .UseNpgsql(Environment.GetEnvironmentVariable("DB_CONNSTRING"))
                                                             .EnableSensitiveDataLogging()
                                                             );
        }

        public void ConfigureSwagger(IServiceCollection services)
        {
            // Configurando o serviço de documentação do Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "Bahngleis API",
                        Version = "v1",
                        Description = "Bahngleis API"

                    });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                c.IncludeXmlComments(xmlPath);
            });
        }



        private void ConfigureDpInjection(IServiceCollection services)
        {
            new InjectionFactory(services);

            services.AddScoped<IOrderRepository<NewOrderModel>, OrderManager>();
            services.AddScoped<ILogRepository<LogModel>, LogManager>();
            services.AddScoped<IDashboardRepository, DashboardManager>();
        }

        public void ConfigureMetrics(IServiceCollection services)
        {
            //-----------WEB METRICS-----------
            var metrics = AppMetrics.CreateDefaultBuilder()
              .Report.ToInfluxDb(
                  options =>
                  {
                      options.InfluxDb.BaseUri = new Uri(Environment.GetEnvironmentVariable("INFLUX_SERVER"));
                      options.InfluxDb.Database = Environment.GetEnvironmentVariable("INFLUX_DB");
                      //options.InfluxDb.Consistenency = "consistency";
                      options.InfluxDb.UserName = Environment.GetEnvironmentVariable("INFLUX_USER");
                      options.InfluxDb.Password = Environment.GetEnvironmentVariable("INFLUX_PASS");
                  })
              .Configuration.Configure(
                      option =>
                      {
                          option.Enabled = true;
                          option.ReportingEnabled = true;
                          option.DefaultContextLabel = "Bahn";
                          option.GlobalTags = new GlobalMetricTags {
                              { "app", "bahn" },
                              { "env", "de" },
                              { "server", Environment.MachineName }
                          };
                      })
              .Build();

            services.AddMetrics(metrics);
            services.AddMetricsTrackingMiddleware(options =>
            {
                options.ApdexTrackingEnabled = true;
                options.OAuth2TrackingEnabled = true;
                options.ApdexTSeconds = 2;
                options.IgnoredHttpStatusCodes = new List<int> { 302, 400, 401 };
            });

            //-----------ENDPOINTS-----------
            services.AddMetricsEndpoints(options =>
            {
                options.MetricsEndpointEnabled = true;
                options.MetricsTextEndpointEnabled = true;
                options.EnvironmentInfoEndpointEnabled = true;
            });

            //-----------HEALTH-----------
            var healthMetrics = AppMetricsHealth.CreateDefaultBuilder()
                                                .HealthChecks.RegisterFromAssembly(services)
                                                .BuildAndAddTo(services);
            services.AddHealth(healthMetrics);
            services.AddHealthEndpoints(options =>
            {
                options.HealthEndpointEnabled = true;
                options.PingEndpointEnabled = true;
            });

            services.AddMetricsReportingHostedService();

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Formatting = Formatting.Indented;
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            }).AddMetrics();
        }

        public void ConfigureCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    build =>
                    {
                        build
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ContextModels>())
                {
                    context.Database.Migrate();
                }
            }
        }


    }
}
