﻿using Bahngleis.Models.Repository;
using Domain;
using Models;
using Models.Verweis;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bahngleis.Services
{
    public class OrderService : InjectionBase
    {
        private readonly IOrderRepository<NewOrderModel> _dataRepository;

        public OrderService(IOrderRepository<NewOrderModel> dataRepository, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _dataRepository = dataRepository;
        }

        public async void ScheduleOrder()
        {
            //List<NewEcommerceModel> ecommerces = await VerweisFacade.Repo.GetEcommerces();            
            List<NewEcommerceModel> ecommerces = new  List<NewEcommerceModel> {

                new NewEcommerceModel
                {
                    Company = new NewCompanyModel
                    {
                        CompanyID = 1,
                        Name = "g2w"
                    },
                    EcommerceID = 1,
                    Name = "teste",
                    Password = "password123",
                    Platform = new NewPlatformModel
                    {
                        PlatformID = 1,
                        Name = "Magento"
                    },
                    Url = "https://docker-magento.bringit.com.br/api/v2_soap",
                    User = "bahn"
                }
             };





            foreach (var ecommerce in ecommerces.Where(s => s.Platform.Name.ToUpper() == "MAGENTO"))
            {
                var ordersMagento = await BahnMagentoFacade.Repo.GetOrdersByStatus(ecommerce.EcommerceID, "processing");

                var ordersInDB = _dataRepository.GetList(null, null, ecommerce.EcommerceID, null);

                foreach (var orderMagento in ordersMagento)
                {
                    NewOrderModel order = await BahnMagentoFacade.Repo.CastOrder(ecommerce.EcommerceID, orderMagento.ToString());

                    _dataRepository.Add(order);

                }
            }
        }
    }
}