﻿using Bahngleis.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Bahngleis.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardRepository _dashboardRepository;

        public DashboardController(IDashboardRepository dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }


        [HttpGet]
        [Route("DashboardInfo")]
        public ActionResult DashboardInfo()
        {
            try
            {
                var data = _dashboardRepository.DashboardInfo();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("RequeueOrder")]
        public ActionResult RequeueOrder(int orderId)
        {
            try
            {
                var data = _dashboardRepository.RequeueOrder(orderId).Result;
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}

