﻿using Bahngleis.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Threading.Tasks;

namespace Bahngleis.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly ILogRepository<LogModel> _dataRepository;

        public LogController(ILogRepository<LogModel> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpPost]
        public ActionResult SaveLog([FromBody] LogModel Log)
        {
            try
            {

                Log.Date = DateTime.Now;

                _dataRepository.Add(Log);
                return Ok();
            }

            catch (Exception exc)
            {
                return BadRequest("Error - " + exc.Message);
            }

        }

        [HttpGet("{OrderNumber}")]
        public async Task<ActionResult> GetLog(string OrderNumber)
        {
            try
            {
                return Ok(await _dataRepository.Get(OrderNumber));
            }
            catch
            {
                return BadRequest("Falha ao buscar o log");
            }
        }

        [HttpGet("GetTypes")]
        public async Task<ActionResult> GetTypes()
        {
            try
            {
                return Ok(await _dataRepository.GetTypes());
            }
            catch
            {
                return BadRequest("Falha ao buscar os tipos de log");
            }
        }
    }
}
