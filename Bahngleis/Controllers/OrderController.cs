﻿using Bahngleis.Models;
using Bahngleis.Models.Repository;
using Bahngleis.Services;
using Infrastructure.Clients.Bahnhof;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bahngleis.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        #region Atributos
        private readonly OrderService _integrationService;
        private readonly IOrderRepository<NewOrderModel> _dataRepository;

        public OrderController(IOrderRepository<NewOrderModel> dataRepository, ContextModels context, IServiceProvider serviceProvider)
        {
            _integrationService = new OrderService(dataRepository, serviceProvider);
            _dataRepository = dataRepository;
        }
        #endregion

        #region Métodos GET
        /// <summary>
        /// Retorna um pedido em específico cadastrado no BD do mOne
        /// </summary>
        /// <param name="OrderNumber">Número do pedido</param>
        /// <param name="SiteName">Nome ca loja do e-commerce</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOrder")]
        public ActionResult GetOrder([FromQuery] string OrderNumber, int ecommerceID)
        {
            try
            {
                var order = _dataRepository.Get(OrderNumber, ecommerceID).Result;

                return Ok(order);
            }
            catch (Exception exc)
            {
                return BadRequest(exc.Message);
            }
        }

        /// <summary>
        /// Retorna uma lista de pedido cadastrados no BD do mOne
        /// </summary>
        /// <param name="dataIni">Filtro de data - início</param>
        /// <param name="dataFim">Filtro de data - fim</param>
        /// <param name="siteName">Nome do e-commerce</param>
        /// <param name="storeName">Nome do marketplace</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOrdersList")]
        public ActionResult GetOrdersList([FromQuery] int ecommerceID, DateTime? dataIni = null, DateTime? dataFim = null, List<string> ecommerceStatus = null)
        {
            try
            {
                List<NewOrderModel> list = _dataRepository.GetList(dataIni, dataFim, ecommerceID, ecommerceStatus).Result;

                if (list.Count() > 0)
                {
                    return Ok(list);
                }

                return Ok("Não existem pedidos com esses filtros");
            }
            catch (Exception exc)
            {
                return BadRequest(exc);
            }
        }


        /// <summary>
        /// Retorna uma lista de pedidos cadastrados no BD do mOne após aplicar os filtros informados pelo usuário
        /// </summary>
        /// <param name="orderNum">N° do pedido</param>
        /// <param name="storeName">Nome da loja</param>
        /// <param name="channelOrder">N° do pedido no Marketplace</param>
        /// <param name="createdFrom">Data de criação do pedido (de)</param>
        /// <param name="createdTo">Data de criação do pedido (até)</param>
        /// <param name="integratedFrom">Data de integração do pedido (de)</param>
        /// <param name="integratedTo">Data de integração do pedido (até)</param>
        /// <param name="orderStatus">Status do pedido</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FilterOrders")]
        public ActionResult FilterOrders([FromQuery] string orderNumber, DateTime? from, DateTime? to, List<string> status, int ecommerceId)
        {
            try
            {
                List<NewOrderModel> list = _dataRepository.FilterOrders(orderNumber, from, to, status, ecommerceId).Result;

                return Ok(list);
            }
            catch (Exception exc)
            {
                return BadRequest(exc);
            }
        }

        /// <summary>
        /// Retorna os pedidos sem determinado documento, buscando pelo tipo do documento
        /// </summary>
        /// <param name="documentType">Id do pedido</param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetOrdersWithoutDocument")]
        //public ActionResult GetOrdersWithoutDocument([FromQuery] string documentType)
        //{
        //    try
        //    {
        //        var order = _dataRepository.GetOrdersWithoutDocument(documentType).Result;

        //        return Ok(order);
        //    }
        //    catch (Exception exc)
        //    {
        //        return BadRequest(exc.Message);
        //    }
        //}
        #endregion

        #region Métodos PUT

        [HttpPut("UpdateOrder/{id}")]
        [HttpPatch("UpdateOrder/{id}")]
        public async Task<ActionResult> UpdateOrder(int id, [FromBody] NewOrderModel order, [FromQuery] bool requeue = false)
        {
            try
            {
                if (order == null)
                {
                    return BadRequest("Order is null.");
                }

                _dataRepository.Update(order);

                if (requeue)
                {
                    BahnhofClient _bahnhofClient = new BahnhofClient();

                    object query = new
                    {
                        order.Number,
                        SiteName = order.EcommerceID
                    };

                    await _bahnhofClient.PostByQueryAsync(query, "CreateMissingDocuments");
                }

                return Ok();
            }

            catch (Exception exc)
            {
                return BadRequest("Error - " + exc.Message);
            }
        }
        #endregion
    }


}
