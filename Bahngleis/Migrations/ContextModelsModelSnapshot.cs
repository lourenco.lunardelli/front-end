﻿// <auto-generated />
using Bahngleis.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;

namespace Bahngleis.Migrations
{
    [DbContext(typeof(ContextModels))]
    partial class ContextModelsModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Models.DocumentType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("DocumentType");
                });

            modelBuilder.Entity("Models.LogModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("JobID");

                    b.Property<int?>("LogTypeID");

                    b.Property<string>("Message");

                    b.Property<string>("Number");

                    b.Property<string>("State");

                    b.HasKey("ID");

                    b.HasIndex("LogTypeID");

                    b.ToTable("Logs");
                });

            modelBuilder.Entity("Models.LogTypeModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("LogType");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            Name = "Parceiro de Negócio"
                        },
                        new
                        {
                            ID = 2,
                            Name = "Pedido"
                        },
                        new
                        {
                            ID = 3,
                            Name = "Lista de Separação"
                        },
                        new
                        {
                            ID = 4,
                            Name = "Nota Fiscal"
                        },
                        new
                        {
                            ID = 5,
                            Name = "Contas a Receber"
                        },
                        new
                        {
                            ID = 6,
                            Name = "Agendamento de pedido"
                        },
                        new
                        {
                            ID = 7,
                            Name = "Atualizador de pedidos"
                        },
                        new
                        {
                            ID = 8,
                            Name = "Atualizador de pedidos MAGENTO"
                        },
                        new
                        {
                            ID = 9,
                            Name = "Agendador de pedidos faltantes"
                        },
                        new
                        {
                            ID = 10,
                            Name = "Não mapeado"
                        });
                });

            modelBuilder.Entity("Models.NewAddressModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AddressType");

                    b.Property<string>("City");

                    b.Property<string>("Complement");

                    b.Property<string>("Country");

                    b.Property<string>("District");

                    b.Property<string>("IbgeCode");

                    b.Property<string>("Mobile");

                    b.Property<string>("Name");

                    b.Property<string>("Number");

                    b.Property<string>("Phone");

                    b.Property<string>("Street");

                    b.Property<string>("TaxIdentification");

                    b.Property<string>("UF");

                    b.Property<string>("ZipCode");

                    b.HasKey("ID");

                    b.ToTable("Address");
                });

            modelBuilder.Entity("Models.NewCustomerModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AddressID");

                    b.Property<string>("Alias");

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("PhoneNumber");

                    b.Property<int?>("ResellerID");

                    b.Property<string>("TaxIdentification");

                    b.HasKey("ID");

                    b.HasIndex("AddressID");

                    b.HasIndex("ResellerID");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("Models.NewDocumentsModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("DocumentTypeID");

                    b.Property<int?>("Entry");

                    b.Property<string>("JobID");

                    b.Property<bool>("JobSucceeded");

                    b.Property<int?>("NewOrderModelID");

                    b.HasKey("ID");

                    b.HasIndex("DocumentTypeID");

                    b.HasIndex("NewOrderModelID");

                    b.ToTable("Document");
                });

            modelBuilder.Entity("Models.NewOrderModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Channel");

                    b.Property<int>("CustomerEcommerceID");

                    b.Property<int?>("CustomerID");

                    b.Property<int>("EcommerceID");

                    b.Property<DateTime>("ImportedDate");

                    b.Property<DateTime>("IntegratedDate");

                    b.Property<string>("MarktplaceID");

                    b.Property<string>("Number");

                    b.Property<int?>("PaymentID");

                    b.Property<DateTime>("PurchaseDate");

                    b.Property<int?>("ShippingID");

                    b.Property<int?>("StatusID");

                    b.HasKey("ID");

                    b.HasIndex("CustomerID");

                    b.HasIndex("PaymentID");

                    b.HasIndex("ShippingID");

                    b.HasIndex("StatusID");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("Models.NewPaymentModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AddressID");

                    b.Property<string>("Currency");

                    b.Property<double>("Discount");

                    b.Property<DateTime>("DueDate");

                    b.Property<int>("Installments");

                    b.Property<string>("Method");

                    b.Property<double>("Total");

                    b.Property<string>("TransactionCode");

                    b.HasKey("ID");

                    b.HasIndex("AddressID");

                    b.ToTable("Payment");
                });

            modelBuilder.Entity("Models.NewProductModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Discount");

                    b.Property<string>("Name");

                    b.Property<int?>("NewOrderModelID");

                    b.Property<double>("Price");

                    b.Property<int>("Quantity");

                    b.Property<string>("SKU");

                    b.HasKey("ID");

                    b.HasIndex("NewOrderModelID");

                    b.ToTable("Product");
                });

            modelBuilder.Entity("Models.NewShippingModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AddressID");

                    b.Property<DateTime>("DueDate");

                    b.Property<bool>("IsDropShipping");

                    b.Property<string>("Method");

                    b.Property<double>("Price");

                    b.Property<double>("QuotedPrice");

                    b.HasKey("ID");

                    b.HasIndex("AddressID");

                    b.ToTable("Shipping");
                });

            modelBuilder.Entity("Models.NewStatusModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("OrderStatus");
                });

            modelBuilder.Entity("Models.ResellerModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CorporateName");

                    b.Property<string>("SellerName");

                    b.Property<string>("StateRegistry");

                    b.HasKey("ID");

                    b.ToTable("Reseller");
                });

            modelBuilder.Entity("Models.LogModel", b =>
                {
                    b.HasOne("Models.LogTypeModel", "LogType")
                        .WithMany()
                        .HasForeignKey("LogTypeID");
                });

            modelBuilder.Entity("Models.NewCustomerModel", b =>
                {
                    b.HasOne("Models.NewAddressModel", "Address")
                        .WithMany()
                        .HasForeignKey("AddressID");

                    b.HasOne("Models.ResellerModel", "Reseller")
                        .WithMany()
                        .HasForeignKey("ResellerID");
                });

            modelBuilder.Entity("Models.NewDocumentsModel", b =>
                {
                    b.HasOne("Models.DocumentType", "DocumentType")
                        .WithMany()
                        .HasForeignKey("DocumentTypeID");

                    b.HasOne("Models.NewOrderModel")
                        .WithMany("Documents")
                        .HasForeignKey("NewOrderModelID");
                });

            modelBuilder.Entity("Models.NewOrderModel", b =>
                {
                    b.HasOne("Models.NewCustomerModel", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerID");

                    b.HasOne("Models.NewPaymentModel", "Payment")
                        .WithMany()
                        .HasForeignKey("PaymentID");

                    b.HasOne("Models.NewShippingModel", "Shipping")
                        .WithMany()
                        .HasForeignKey("ShippingID");

                    b.HasOne("Models.NewStatusModel", "Status")
                        .WithMany()
                        .HasForeignKey("StatusID");
                });

            modelBuilder.Entity("Models.NewPaymentModel", b =>
                {
                    b.HasOne("Models.NewAddressModel", "Address")
                        .WithMany()
                        .HasForeignKey("AddressID");
                });

            modelBuilder.Entity("Models.NewProductModel", b =>
                {
                    b.HasOne("Models.NewOrderModel")
                        .WithMany("Products")
                        .HasForeignKey("NewOrderModelID");
                });

            modelBuilder.Entity("Models.NewShippingModel", b =>
                {
                    b.HasOne("Models.NewAddressModel", "Address")
                        .WithMany()
                        .HasForeignKey("AddressID");
                });
#pragma warning restore 612, 618
        }
    }
}
