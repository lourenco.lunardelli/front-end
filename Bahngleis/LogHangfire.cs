﻿using Bahngleis.Models.DataManager;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using Models;
using System;
using System.Linq;

namespace Bahngleis
{
    public class LogHangfire : JobFilterAttribute, IElectStateFilter, IServerFilter
    {
        //STARTED
        public void OnPerforming(PerformingContext context)
        {
            string number = context.BackgroundJob.Job.Args.Count() > 0 ? context.BackgroundJob.Job.Args.FirstOrDefault().ToString() : null;

            var log = new LogModel
            {
                Number = number,
                Message = $"Iniciado o agendamento do pedido {number}",
                JobID = context.BackgroundJob.Id,
                Date = DateTime.Now,
                LogType = GetLogTypeModel(context.BackgroundJob.Job.Method.Name),
                State = "Started"
            };

            //LogManager logManager = new LogManager();
            //logManager.Add

            //Bahngleis.HttpClient.PostAsync(log).Wait();
        }

        //FINISHED
        public void OnPerformed(PerformedContext context)
        {
            if (context.Exception is null)
            {
                string number = context.BackgroundJob.Job.Args.Count() > 0 ? context.BackgroundJob.Job.Args.FirstOrDefault().ToString() : null;

                var log = new LogModel
                {
                    Number = number,
                    Message = $"Finalizado o agendamento do pedido {number}",
                    JobID = context.BackgroundJob.Id,
                    Date = DateTime.Now,
                    LogType = GetLogTypeModel(context.BackgroundJob.Job.Method.Name),
                    State = "Success"
                };

                //Bahngleis.HttpClient.PostAsync(log).Wait();
            }
        }

        //ERRO
        public void OnStateElection(ElectStateContext context)
        {
            var failedState = context.CandidateState as FailedState;
            if (failedState != null)
            {
                string number = context.BackgroundJob.Job.Args.Count() > 0 ? context.BackgroundJob.Job.Args.FirstOrDefault().ToString() : null;

                var log = new LogModel
                {
                    Number = number,
                    Message = failedState.Exception.Message,
                    JobID = context.BackgroundJob.Id,
                    Date = DateTime.Now,
                    LogType = GetLogTypeModel(context.BackgroundJob.Job.Method.Name),
                    State = "Error"
                };

                //Bahngleis.HttpClient.PostAsync(log).Wait();
            }
        }

        public LogTypeModel GetLogTypeModel(string method)
        {
            LogTypeModel logType = new LogTypeModel();

            switch (method)
            {
                case "ScheduleOrder":
                    //logType = Bahngleis.HttpClient.GetAsync<LogTypeModel>().Result.Where(s => s.Name == "Agendamento de pedido").First();
                    break;

                default:
                    //logType = Bahngleis.HttpClient.GetAsync<LogTypeModel>().Result.Where(s => s.Name == "Não Mapeado").First();
                    break;
            }

            return logType;
        }
    }
}