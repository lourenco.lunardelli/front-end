﻿using Microsoft.EntityFrameworkCore;
using Models;
using System.Collections.Generic;

namespace Bahngleis.Models
{
    public class ContextModels : DbContext
    {
        public ContextModels(DbContextOptions<ContextModels> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //-----------------------DOCUMENT TYPES-----------------------
            List<DocumentType> DocumentTypes = new List<DocumentType>
            {
                new DocumentType() { DocumentTypeID = 1, Name = "Parceiro de Negócio" },
                new DocumentType() { DocumentTypeID = 2, Name = "Pedido" },
                new DocumentType() { DocumentTypeID = 3, Name = "Lista de Separação" },
                new DocumentType() { DocumentTypeID = 4, Name = "Nota Fiscal" },
                new DocumentType() { DocumentTypeID = 5, Name = "Contas a Receber" }
            };

            builder.Entity<DocumentType>().HasData(DocumentTypes);

            //-----------------------LOG TYPES-----------------------
            List<LogTypeModel> LogTypes = new List<LogTypeModel>
            {
                new LogTypeModel() { LogTypeID = 1, Name = "Parceiro de Negócio" },
                new LogTypeModel() { LogTypeID = 2, Name = "Pedido" },
                new LogTypeModel() { LogTypeID = 3, Name = "Lista de Separação" },
                new LogTypeModel() { LogTypeID = 4, Name = "Nota Fiscal" },
                new LogTypeModel() { LogTypeID = 5, Name = "Contas a Receber" },
                new LogTypeModel() { LogTypeID = 6, Name = "Agendamento de pedido" },
                new LogTypeModel() { LogTypeID = 7, Name = "Atualizador de pedidos" },
                new LogTypeModel() { LogTypeID = 8, Name = "Atualizador de pedidos MAGENTO" },
                new LogTypeModel() { LogTypeID = 9, Name = "Agendador de pedidos faltantes" },
                new LogTypeModel() { LogTypeID = 10, Name = "Não mapeado" }
            };

            builder.Entity<LogTypeModel>().HasData(DocumentTypes);
        }

        public virtual DbSet<NewCustomerModel> Customer { get; set; }
        public virtual DbSet<NewOrderModel> Order { get; set; }
        public virtual DbSet<NewStatusModel> OrderStatus { get; set; }
        public virtual DbSet<LogTypeModel> LogType { get; set; }
        public virtual DbSet<LogModel> Logs { get; set; }
        public virtual DbSet<NewPaymentModel> Payment { get; set; }
        public virtual DbSet<NewProductModel> Product { get; set; }
        public virtual DbSet<NewShippingModel> Shipping { get; set; }
        public virtual DbSet<NewAddressModel> Address { get; set; }
        public virtual DbSet<ResellerModel> Reseller { get; set; }
        public virtual DbSet<NewDocumentsModel> Document { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }

    }
}
