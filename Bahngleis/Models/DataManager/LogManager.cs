﻿using Bahngleis.Models.Repository;
using Microsoft.EntityFrameworkCore;
using Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bahngleis.Models.DataManager
{
    public class LogManager : ILogRepository<LogModel>
    {
        private readonly ContextModels _context;

        public LogManager(ContextModels context)
        {
            _context = context;
        }

        public async Task<List<LogModel>> Get(string OrderNumber)
        {
            return await _context.Logs.Where(s => s.Number == OrderNumber).OrderBy(s => s.ID).ToListAsync();
        }

        public void Add(LogModel entity)
        {
            _context.Entry(entity.LogType).State = EntityState.Unchanged;
            _context.Logs.Add(entity);
            _context.SaveChanges();
        }


        public async Task<List<LogTypeModel>> GetTypes()
        {
            return await _context.LogType.ToListAsync();
        }
    }
}
