﻿using Bahngleis.Models.Repository;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bahngleis.Models.DataManager
{
    public class DashboardManager : IDashboardRepository
    {
        readonly ContextModels _context;
        
        public DashboardManager(ContextModels context)
        {
            _context = context;
        }

        public DashboardInfoModel DashboardInfo()
        {
            try
            {
                var dashboardInfo = new DashboardInfoModel
                {
                    ChartData = GetChartData(),
                    CardsData = GetCardsData()
                };

                return dashboardInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<NewOrderModel>> FilterOrders(string orderNumber, DateTime? from, DateTime? to, List<string> status, int ecommerceId)
        {
            var query = _context.Order
                .Select(o => new NewOrderModel
                {
                    OrderID = o.OrderID,
                    Number = o.Number,
                    PurchaseDate = o.PurchaseDate,
                    IntegratedDate = o.IntegratedDate,
                    ImportedDate = o.ImportedDate,
                    Documents = o.Documents,
                    Status = o.Status
                });

            if (!string.IsNullOrEmpty(orderNumber))
                query = query.Where(o => o.Number == orderNumber);

            if (from != null && to != null)
                query = query.Where(o => o.PurchaseDate >= from && o.PurchaseDate <= to);

            else if (from != null && to == null)
                query = query.Where(o => o.PurchaseDate >= from);

            else if (from == null && to != null)
                query = query.Where(o => o.PurchaseDate <= to);

            if (ecommerceId > 0)
                query = query.Where(o => o.EcommerceID == ecommerceId);

            //if (StoreName != null)
            //    query = query.Where(s => s.StoreName == StoreName);

            if (status.Count() > 0)
                query = query.Where(o => status.Contains(o.Status.Name));

            return await query.OrderByDescending(o => o.PurchaseDate).ToListAsync();
        }

        public async Task<NewOrderModel> GetOrderById(int OrderId)
        {
            var order = await _context.Order
                .Where(o => o.OrderID == OrderId)
                .Include(s => s.Shipping)
                .Include(s => s.Shipping.Address)
                .Include(p => p.Payment)
                .Include(s => s.Payment.Address)
                .Include(c => c.Customer)
                .Include(c => c.Customer.Address)
                .Include(s => s.Products)
                .FirstOrDefaultAsync();

            return order;
        }

        public async Task<string> RequeueOrder(int orderId)
        {
            var order = _context.Order.Find(orderId);

            if (order.Status.Name != "integrated" && order.Status.Name != "complete" && order.Status.Name != "canceled")
            {
                //if (!string.IsNullOrEmpty(order.LastJobId))
                if (!string.IsNullOrEmpty(order.Channel))
                {
                    await Infrastructure.Facades.Bahnhof.HttpClient.PostByQueryAsync(new { orderId }, "RequeueOrder");

                    return "Requeue order sended";
                }
                else
                {
                    object query = new
                    {
                        order.Number,
                        order.EcommerceID
                    };

                    await Infrastructure.Facades.Bahnhof.HttpClient.PostByQueryAsync(query, "CreateMissingDocuments");
                    return "Requeue order sended";
                }
            }
            else
            {
                return "Cannot requeue this order";
            }
        }


        #region Métodos privados
        private ChartDataModel GetChartData()
        {
            try
            {
                var chartData = new ChartDataModel
                {
                    WeeklyData = GetWeeklyData(),
                    BiweeklyData = GetBiweeklyData(),
                    MonthlyData = GetMonthlyData()
                };

                return chartData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ChartInfoModel GetWeeklyData()
        {
            var chartInfo = new ChartInfoModel
            {
                Labels = new List<string>(),
                Values = new List<int>()
            };


            var from = DateTime.Today.AddDays(-7).Date;
            var to = DateTime.Today.AddDays(1).Date;

            while (from < to)
            {
                var orders = _context.Order.Where(o => o.IntegratedDate != null);

                var ordersCount = orders.Where(o => o.IntegratedDate.Date.ToString("dd/MM") == from.ToString("dd/MM") &&
                    (o.Status.Name == "complete" || o.Status.Name == "integrated")).Count();


                chartInfo.Labels.Add(from.ToString("dd/MM"));
                chartInfo.Values.Add(ordersCount);

                from = from.AddDays(1);
            }

            return chartInfo;
        }

        private ChartInfoModel GetBiweeklyData()
        {
            var chartInfo = new ChartInfoModel
            {
                Labels = new List<string>(),
                Values = new List<int>()
            };


            var from = DateTime.Today.AddDays(-15).Date;
            var to = DateTime.Today.AddDays(1).Date;

            while (from < to)
            {
                var orders = _context.Order.Where(o => o.IntegratedDate != null);

                var ordersCount = orders.Where(o => o.IntegratedDate.Date.ToString("dd/MM") == from.ToString("dd/MM") &&
                    (o.Status.Name == "complete" || o.Status.Name == "integrated")).Count();


                chartInfo.Labels.Add(from.ToString("dd/MM"));
                chartInfo.Values.Add(ordersCount);

                from = from.AddDays(1);
            }

            return chartInfo;
        }

        private ChartInfoModel GetMonthlyData()
        {
            var chartInfo = new ChartInfoModel
            {
                Labels = new List<string>(),
                Values = new List<int>()
            };


            var from = DateTime.Today.AddDays(-30).Date;
            var to = DateTime.Today.AddDays(1).Date;

            while (from < to)
            {
                var orders = _context.Order.Where(o => o.IntegratedDate != null);

                var ordersCount = orders.Where(o => o.IntegratedDate.Date.ToString("dd/MM") == from.ToString("dd/MM") &&
                    (o.Status.Name == "complete" || o.Status.Name == "integrated")).Count();


                chartInfo.Labels.Add(from.ToString("dd/MM"));
                chartInfo.Values.Add(ordersCount);

                from = from.AddDays(1);
            }

            return chartInfo;
        }

        private CardsDataModel GetCardsData()
        {
            try
            {
                var cardsData = new CardsDataModel();

                cardsData.IntegratedToday = _context.Order.Where(o => o.IntegratedDate >= DateTime.Today && o.IntegratedDate <= DateTime.Now &&
                         (o.Status.Name == "integrated" || o.Status.Name == "complete")).Count();

                cardsData.IntegratedLastHour = _context.Order.Where(o => o.IntegratedDate >= DateTime.Now.AddHours(-1) && o.IntegratedDate <= DateTime.Now &&
                         (o.Status.Name == "integrated" || o.Status.Name == "complete")).Count();

                cardsData.OrdersPending = _context.Order.Where(o => o.Status.Name == "scheduled" || o.Status.Name == "processing").Count();

                cardsData.OrdersWarning = _context.Order.Where(o => o.Status.Name == "error" || o.Status.Name == "holded").Count();


                return cardsData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
