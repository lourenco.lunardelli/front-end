﻿using Bahngleis.Models.Repository;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bahngleis.Models.DataManager
{
    public class OrderManager : IOrderRepository<NewOrderModel>
    {
        #region Atributos
        private readonly ContextModels _context;
        #endregion

        #region Construtor
        public OrderManager(ContextModels context)
        {
            _context = context;
        }
        #endregion 


        public async Task<NewOrderModel> Get(string orderNumber, int ecommerceId)
        {
            var order = await _context.Order
                .Where(o => o.Number == orderNumber && o.EcommerceID == ecommerceId)
                .Include(py => py.Payment)
                .Include(pa => pa.Payment.Address)
                .Include(sh => sh.Shipping)
                .Include(sa => sa.Shipping.Address)
                .Include(c => c.Customer)
                .Include(ca => ca.Customer.Address)
                .Include(pr => pr.Products)
                .Include(d => d.Documents)
                .FirstOrDefaultAsync();

            return order;
        }

        public async Task<List<NewOrderModel>> GetList(DateTime? from, DateTime? to, int ecommerceId, List<string> status)
        {
            var query = _context.Order.AsQueryable();

            if (from != null && to != null)
                query = query.Where(o => o.ImportedDate >= from && o.ImportedDate <= to);

            else if (from != null && to == null)
                query = query.Where(s => s.ImportedDate >= from);

            else if (from == null && to != null)
                query = query.Where(s => s.ImportedDate <= to);



            if (ecommerceId > 0)
                query = query.Where(s => s.EcommerceID == ecommerceId);

            //if (StoreName != null)
            //    query = query.Where(s => s.StoreName == StoreName);

            if (status.Count() > 0)
                query = query.Where(o => status.Contains(o.Status.Name));

            return await query.ToListAsync();
        }


        public async Task<List<NewOrderModel>> FilterOrders(string orderNumber, DateTime? from, DateTime? to, List<string> status, int ecommerceId)
        {
            var query = _context.Order
                .Select(o => new NewOrderModel
                {
                    OrderID = o.OrderID,
                    Number = o.Number,
                    PurchaseDate = o.PurchaseDate,
                    IntegratedDate = o.IntegratedDate,
                    ImportedDate = o.ImportedDate,
                    Documents = o.Documents,
                    Status = o.Status
                });

            if (!string.IsNullOrEmpty(orderNumber))
                query = query.Where(o => o.Number == orderNumber);

            if (from != null && to != null)
                query = query.Where(o => o.PurchaseDate >= from && o.PurchaseDate <= to);

            else if (from != null && to == null)
                query = query.Where(o => o.PurchaseDate >= from);

            else if (from == null && to != null)
                query = query.Where(o => o.PurchaseDate <= to);

            if (ecommerceId > 0)
                query = query.Where(o => o.EcommerceID == ecommerceId);

            //if (StoreName != null)
            //    query = query.Where(s => s.StoreName == StoreName);

            if (status.Count() > 0)
                query = query.Where(o => status.Contains(o.Status.Name));

            return await query.OrderByDescending(o => o.PurchaseDate).ToListAsync();
        }

        //public async Task<List<NewOrderModel>> GetOrdersWithoutDocument(string documentType)
        //{
        //    var query = _context.Order.AsQueryable();

        //    switch (documentType)
        //    {
        //        case "order":
        //            query = query.Where(s => s.ORDRDocNum == 0);
        //            break;
        //        case "picking":
        //            query = query.Where(s => s.PListNum == 0 || s.PListNum == null);
        //            break;
        //        case "invoice":
        //            query = query.Where(s => s.OINVDocNum == 0);
        //            break;
        //        case "payment":
        //            query = query.Where(s => s.ORCTDocNum == 0);
        //            break;
        //        case "tracking":
        //            query = query.Where(s => s.MEtrackingNum == "");
        //            break;
        //        default:
        //            break;
        //    }

        //    return await query.Where(s => s.EcommerceStatus != "canceled").OrderByDescending(o => o.OrderCreatedAt).ToListAsync();
        //}

        
        public void Add(NewOrderModel entity)
        {
            _context.Order.Add(entity);
            _context.SaveChanges();
        }

        public void Update(NewOrderModel entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.Entry(entity.Customer).State = EntityState.Modified;

            if (entity.Customer.Address.AddressID != entity.Payment.Address.AddressID)
                _context.Entry(entity.Customer.Address).State = EntityState.Modified;

            _context.Entry(entity.Payment).State = EntityState.Modified;
            foreach (var product in entity.Products)
            {
                _context.Entry(product).State = EntityState.Modified;
            }
            _context.Entry(entity.Shipping).State = EntityState.Modified;

            _context.Entry(entity.Shipping.Address).State = EntityState.Modified;

            _context.Entry(entity.Payment.Address).State = EntityState.Modified;

            _context.SaveChanges();

            //_context.Update(entity);

        }

        public void Delete(NewOrderModel entity)
        {
            _context.Order.Remove(entity);
            _context.SaveChanges();
        }
    }
}
