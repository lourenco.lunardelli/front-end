﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bahngleis.Models.Repository
{
    public interface IOrderRepository<TEntity>
    {
        Task<List<TEntity>> GetList(DateTime? from, DateTime? to, int ecommerceId, List<string> status);
        Task<List<TEntity>> FilterOrders(string orderNumber, DateTime? from, DateTime? to, List<string> status, int ecommerceId);
        //Task<List<TEntity>> GetOrdersWithoutDocument(string documentType);
        Task<TEntity> Get(string orderNumber, int ecommerceId);
        void Add(TEntity entity);
        void Update(TEntity dbEntity);
        void Delete(TEntity entity);
    }
}
