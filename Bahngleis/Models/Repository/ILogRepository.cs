﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bahngleis.Models.Repository
{
    public interface ILogRepository<TEntity>
    {
        Task<List<TEntity>> Get(string OrderNumber);
        void Add(TEntity entity);

        Task<List<LogTypeModel>> GetTypes();
    }
}
