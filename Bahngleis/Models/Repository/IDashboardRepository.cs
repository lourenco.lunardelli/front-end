﻿using Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bahngleis.Models.Repository
{
    public interface IDashboardRepository
    {
        Task<string> RequeueOrder(int orderId);
        DashboardInfoModel DashboardInfo();

        Task<NewOrderModel> GetOrderById(int OrderId);

        Task<List<NewOrderModel>> FilterOrders(string orderNumber, DateTime? from, DateTime? to, List<string> status, int ecommerceId);
    }
}
