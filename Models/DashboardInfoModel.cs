﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class DashboardInfoModel
    {
        public ChartDataModel ChartData { get; set; }
        public CardsDataModel CardsData { get; set; }
    }

    public class ChartDataModel
    {
        public ChartInfoModel WeeklyData { get; set; }
        public ChartInfoModel BiweeklyData { get; set; }
        public ChartInfoModel MonthlyData { get; set; }
    }

    public class CardsDataModel
    {
        public int IntegratedToday { get; set; }
        public int IntegratedLastHour { get; set; }
        public int OrdersPending { get; set; }
        public int OrdersWarning { get; set; }
    }

    public class ChartInfoModel
    {
        public List<string> Labels { get; set; }
        public List<int> Values { get; set; }
    }
}
