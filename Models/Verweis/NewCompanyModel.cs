﻿using Newtonsoft.Json;

namespace Models.Verweis
{
    public class NewCompanyModel
    {
        [JsonProperty("id")]
        public int CompanyID { get; set; }
        public string Name { get; set; }
    }
}
