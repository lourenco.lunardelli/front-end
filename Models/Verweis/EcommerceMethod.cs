﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Verweis
{
    public class MappedMethod
    {
        public string MappedMethodID { get; set; }
        public string ERPName { get; set; }
        public string HUBName { get; set; }
        public string EcommerceName { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
