﻿using Newtonsoft.Json;

namespace Models.Verweis
{
    public class NewPlatformModel
    {
        [JsonProperty("id")]
        public int PlatformID { get; set; }
        public string Name { get; set; }
    }
}
