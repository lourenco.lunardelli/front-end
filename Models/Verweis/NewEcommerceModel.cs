﻿using Newtonsoft.Json;

namespace Models.Verweis
{
    public class NewEcommerceModel
    {
        [JsonProperty("id")]
        public int EcommerceID { get; set; }
        public string Name { get; set; }
        public virtual NewCompanyModel Company { get; set; }
        public virtual NewPlatformModel Platform { get; set; }
        public string Url { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
