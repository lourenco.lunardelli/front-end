﻿namespace Models
{
    public class NewDocumentsModel
    {
        public int DocumentID { get; set; }
        public string Entry { get; set; }
        public string JobID { get; set; }
        public bool JobSucceeded { get; set; }

        public virtual DocumentType DocumentType { get; set; }
    }
}
