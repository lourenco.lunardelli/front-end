﻿namespace Models
{
    public class ResellerModel
    {
        public int ResellerID { get; set; }
        public string StateRegistry { get; set; }
        public string CorporateName { get; set; }
        public string SellerName { get; set; }
    }
}
