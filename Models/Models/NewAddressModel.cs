﻿namespace Models
{
    public class NewAddressModel
    {
        public int AddressID { get; set; }
        public string IbgeCode { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string UF { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string TaxIdentification { get; set; }
        public string AddressType { get; set; }

    }
}
