﻿namespace Models
{
    public class NewCustomerModel
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Email { get; set; }
        public string TaxIdentification { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ResellerModel Reseller { get; set; }

        public virtual NewAddressModel Address { get; set; }
    }
}
