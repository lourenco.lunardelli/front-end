﻿using System;

namespace Models
{
    public class NewShippingModel
    {
        public int ShippingID { get; set; }
        public string Method { get; set; }
        public bool IsDropShipping { get; set; }
        public double Price { get; set; }
        public double QuotedPrice { get; set; }
        public DateTime DueDate { get; set; }

        public virtual NewAddressModel Address { get; set; }
    }
}
