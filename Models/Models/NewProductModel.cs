﻿namespace Models
{
    public class NewProductModel
    {
        public int ProductID { get; set; }
        public double Discount { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
    }
}
