﻿namespace Models
{
    public class LogTypeModel
    {
        public int LogTypeID { get; set; }
        public string Name { get; set; }
    }
}
