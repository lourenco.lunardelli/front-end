﻿using System;

namespace Models
{
    public class LogModel
    {
        public int ID { get; set; }
        public string Number { get; set; }
        public string Message { get; set; }
        public string JobID { get; set; }
        public DateTime Date { get; set; }
        public string State { get; set; }

        public virtual LogTypeModel LogType { get; set; }
    }
}
