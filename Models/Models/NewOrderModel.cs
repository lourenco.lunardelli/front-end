﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class NewOrderModel
    {
        public int OrderID { get; set; }
        public int EcommerceID { get; set; }
        public string Channel { get; set; }
        public int CustomerEcommerceID { get; set; }
        public string MarktplaceID { get; set; }
        public string Number { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime IntegratedDate { get; set; }
        public DateTime ImportedDate { get; set; }

        public virtual NewShippingModel Shipping { get; set; }
        public virtual NewPaymentModel Payment { get; set; }
        public virtual NewCustomerModel Customer { get; set; }
        public virtual List<NewProductModel> Products { get; set; }
        public virtual NewStatusModel Status { get; set; }
        public virtual List<NewDocumentsModel> Documents { get; set; }
    }
}
