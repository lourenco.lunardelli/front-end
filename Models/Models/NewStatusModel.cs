﻿namespace Models
{
    public class NewStatusModel
    {
        public int StatusID { get; set; }
        public string Name { get; set; }
    }
}
