﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.PredefinedText
{
    public class PredefinedTextModel
    {
        public int Numerator { get; set; }
        public string TextCode { get; set; }
        public string Text { get; set; }
    }
}
