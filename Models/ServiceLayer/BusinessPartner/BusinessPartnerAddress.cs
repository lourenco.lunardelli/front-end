﻿namespace Models.ServiceLayer.BusinessPartner
{
    public class BusinessPartnerAddress
    {
        public string AddressName { get; set; }
        public string Street { get; set; }
        public string Block { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string BuildingFloorRoom { get; set; }
        public string AddressType { get; set; }
        public object AddressName2 { get; set; }
        public string StreetNo { get; set; }
        public string BPCode { get; set; }
        public int RowNum { get; set; }
        public string U_SKILL_indIEDest { get; set; }
    }
}
