﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Payments
{
    public class WizardPaymentMethodsModel
    {
        public string PaymentMethodCode { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string PaymentMeans { get; set; }
        public string CheckAddress { get; set; }
        public string CheckBankDetails { get; set; }
        public string CollectionAuthorizationCheck { get; set; }
        public string BlockForeignPayment { get; set; }
        public string BlockForeignBank { get; set; }
        public string CurrencyRestriction { get; set; }
        public string PostOfficeBank { get; set; }
        public double MinimumAmount { get; set; }
        public int MaximumAmount { get; set; }
        public string DefaultBank { get; set; }
        public int UserSignature { get; set; }
        public string CreationDate { get; set; }
        public string BankCountry { get; set; }
        public string DefaultAccount { get; set; }
        public string GLAccount { get; set; }
        public string Branch { get; set; }
        public string Format { get; set; }
        public string AgentCollection { get; set; }
        public string SendforAcceptance { get; set; }
        public string GroupByDate { get; set; }
        public string DebitMemo { get; set; }
        public string GroupByPaymentReference { get; set; }
        public string GroupInvoicesbyPay { get; set; }
        public string DueDateSelection { get; set; }
        public string PosttoGLInterimAccount { get; set; }
        public int BankAccountKey { get; set; }
        public string DocType { get; set; }
        public string Accepted { get; set; }
        public string PortfolioID { get; set; }
        public string CurCode { get; set; }
        public string Instruction1 { get; set; }
        public string Instruction2 { get; set; }
        public string PaymentPlace { get; set; }
        public string Active { get; set; }
        public string GroupInvoicesByPayToBank { get; set; }
        public string GroupInvoicesByCurrency { get; set; }
        public int BankChargeRate { get; set; }
        public string MovementCode { get; set; }
        public string DirectDebit { get; set; }

        public Currencyrestriction[] CurrencyRestrictions { get; set; }
    }

    public class Currencyrestriction
    {
        public string PaymentMethodCode { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string Choose { get; set; }
    }


}
