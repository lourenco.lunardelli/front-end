﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Payments
{
    public class PaymentTermsTypesModel
    {
        public int GroupNumber { get; set; }
        public string PaymentTermsGroupName { get; set; }
        public string StartFrom { get; set; }
        public int NumberOfAdditionalMonths { get; set; }
        public int NumberOfAdditionalDays { get; set; }
        public int CreditLimit { get; set; }
        public int GeneralDiscount { get; set; }
        public int InterestOnArrears { get; set; }
        public int PriceListNo { get; set; }
        public int LoadLimit { get; set; }
        public string OpenReceipt { get; set; }
        public string BaselineDate { get; set; }
        public int NumberOfInstallments { get; set; }
    }
}
