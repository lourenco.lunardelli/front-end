﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Countries
{
    public class CountryModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
