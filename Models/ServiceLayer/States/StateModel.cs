﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.States
{
    public class StateModel
    {
        public string Code { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
    }
}
