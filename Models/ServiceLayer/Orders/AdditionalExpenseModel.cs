﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Orders
{
    public class AdditionalExpenseModel
    {
        public int? ExpenseCode { get; set; }
        public int? BaseDocType { get; set; }
        public int? BaseDocEntry { get; set; }
        public int? BaseDocLine { get; set; }
        public double? LineTotal { get; set; }

    }
}
