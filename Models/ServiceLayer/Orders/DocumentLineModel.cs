﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.Orders
{
    //--------------------------------------DOCUMENTS LINES(PRODUCTS)--------------------------------------
    public class DocumentLineModel
    {
        public int? LineNum { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public int? BaseType { get; set; }
        public int? BaseEntry { get; set; }
        public int? BaseLine { get; set; }
        public string CSTCode { get; set; }
        public string PriceAfterVAT { get; set; }
        public string Currency { get; set; }
        public string Usage { get; set; }
        public string WarehouseCode { get; set; }
        public double? UnitPrice { get; set; }
        public double? Quantity { get; set; }
        public double? DiscountPercent { get; set; }
        public string FreeText { get; set; }
        public List<BatchNumbers> BatchNumbers { get; set; }
        public List<BinAllocations> DocumentLinesBinAllocations { get; set; }
        public string U_SKILL_BenefFiscal { get; set; }

    }

    public class BatchNumbers
    {
        public int? BaseLineNumber { get; set; }
        public string BatchNumber { get; set; }
        public decimal? Quantity { get; set; }
    }

    public class BinAllocations
    {
        public int? BaseLineNumber { get; set; }
        public int? BinAbsEntry { get; set; }
        public decimal? Quantity { get; set; }
        public int? SerialAndBatchNumbersBaseLine { get; set; }
    }
}
