﻿namespace Models.ServiceLayer
{
    public class ServiceLayerLoginModel
    {
        public string SessionId { get; set; }
        public string Version { get; set; }
        public int SessionTimeout { get; set; }
    }
}
