﻿using Models.ServiceLayer.Orders;
using System;
using System.Collections.Generic;

namespace Models.ServiceLayer.Invoices
{
    public class InvoiceModel
    {
        public int? BPL_IDAssignedToInvoice { get; set; }
        public DateTime? DocDueDate { get; set; }
        public string CardCode { get; set; }
        public string PaymentMethod { get; set; }
        public int? PaymentGroupCode { get; set; }
        public string OpeningRemarks { get; set; }
        public int? SequenceCode { get; set; }
        public int? SequenceSerial { get; set; }
        public string SeriesString { get; set; }
        public string SequenceModel { get; set; }

        public List<DocumentLineModel> DocumentLines { get; set; }
        public List<AdditionalExpenseModel> DocumentAdditionalExpenses { get; set; }
        public List<Installment> DocumentInstallments { get; set; }
        public TaxExtensionModel TaxExtension { get; set; }
    }

    public class Installment
    {
        public DateTime DueDate { get; set; }
        public double? Total { get; set; }
    }



    //--------------------------------------ORDER UPDATER--------------------------------------
    public class Danfe
    {
        public string OrderNumber { get; set; }
        public string U_ChaveAcesso { get; set; }
        public int DocNum { get; set; }
        public int OrderEntry { get; set; }
        public string Serial { get; set; }
    }

}
