﻿namespace Models.ServiceLayer.Counties
{
    public class CountyModel
    {
        public int AbsId { get; set; }
        public string Code { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Name { get; set; }
        public string TaxZone { get; set; }
        public string IbgeCode { get; set; }
        public string GiaCode { get; set; }
    }
}
