﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.ServiceLayer.Documents
{
    public class SAPDocuments
    {
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public string Cancelled { get; set; }
    }
}