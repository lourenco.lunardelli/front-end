﻿namespace Models.ServiceLayer.Shippings
{
    public class ShippingTypesModel
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
    }
}
