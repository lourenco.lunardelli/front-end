﻿using System.Collections.Generic;

namespace Models.ServiceLayer.Shippings
{
    public class Channels
    {
        ///MAPEAMENTO:
        ///ChannelName, HaveShipping
        public static Dictionary<string, ChannelModel> channels = new Dictionary<string, ChannelModel> {
            { "mercado_livre", new ChannelModel{ ChannelName = "mercado_livre", HaveShipping =  false} },
            { "mercadolivre", new ChannelModel{ ChannelName = "mercadolivre", HaveShipping =  false} },
            { "submarino", new ChannelModel{ ChannelName = "submarino", HaveShipping =  true} },
            { "shoptime", new ChannelModel{ ChannelName = "shoptime", HaveShipping =  true} },
            { "lojas americanas", new ChannelModel{ ChannelName = "lojas americanas", HaveShipping =  true} }
        };

    }

    public class ChannelModel
    {
        public string ChannelName { get; set; }
        public bool HaveShipping { get; set; }
    }
}
