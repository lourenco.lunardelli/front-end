﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.IncomingPayment
{
    public class IncomingPaymentModel
    {
        public int BPLID { get; set; }
        public DateTime DocDate { get; set; }
        public DateTime DueDate { get; set; }
        public string LocalCurrency { get; set; }
        public string CardCode { get; set; }

        public List<CreditCardModel> PaymentCreditCards { get; set; }
        public List<PaymentInvoiceModel> PaymentInvoices { get; set; }
    }

    public class CreditCardModel
    {
        public double AdditionalPaymentSum { get; set; }
        public DateTime CardValidUntil { get; set; }
        public int CreditCard { get; set; }
        public string CreditAcct { get; set; }
        public string VoucherNum { get; set; }
        public string CreditCardNumber { get; set; }
        public int PaymentMethodCode { get; set; }
        public double CreditSum { get; set; }
        public string CreditType { get; set; }
        public DateTime FirstPaymentDue { get; set; }
        public int NumOfPayments { get; set; }
        public string SplitPayments { get; set; }
        public double FirstPaymentSum { get; set; }
    }

    public class PaymentInvoiceModel
    {
        public int DocEntry { get; set; }
        public int InstallmentId { get; set; }
        public string InvoiceType { get; set; }
    }
}
