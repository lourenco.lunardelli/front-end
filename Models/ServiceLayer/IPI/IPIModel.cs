﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ServiceLayer.IPI
{
    public class IPIModel
    {
        public string itemCode { get; set; }
        public double Ipi { get; set; }
    }
}
