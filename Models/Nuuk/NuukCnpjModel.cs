using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Models.Nuuk
{
    public class NuukCnpjModel
    {
        public int Codigo { get; set; }
        public Consulta Consulta { get; set; }
        //        public object Mensagem { get; set; }
        //        public int TotalConsultas { get; set; }
        //        public object Configuracao { get; set; }
        //        public object ListaConsultas { get; set; }
        //        public object ListaConsultasPorEstado { get; set; }
        //        public object Menus { get; set; }   
    }

    //    public class CnaePrincipal
    //    {
    //        public string code { get; set; }
    //        public string text { get; set; }
    //    }

    public class Consulta
    {
        [JsonProperty("cnpj")]
        public string Cnpj { get; set; }
        [JsonProperty("nome_empresarial")]
        public string RazaoSocial { get; set; }
        [JsonProperty("inscricao_estadual")]
        public string InscricaoEstadual { get; set; }
        [JsonProperty("situacao_ie")]
        public string SituacaoIE { get; set; }
        //        public int Id { get; set; }
        //        public string Status { get; set; }
        //        public object Origem { get; set; }
        //        public object Usuario { get; set; }
        //        public string ConsultaCobrada { get; set; }
        //        public string MensagemLog { get; set; }
        //        public string Data { get; set; }
        //public string tipo_inscricao { get; set; }
        //        public string data_situacao_cadastral { get; set; }
        //        public string situacao_cnpj { get; set; }
        //        public string nome_fantasia { get; set; }
        //        public string data_inicio_atividade { get; set; }
        //        public string regime_tributacao { get; set; }
        //        public string informacao_ie_como_destinatario { get; set; }
        //        public string porte_empresa { get; set; }
        //        public object data_fim_atividade { get; set; }
        //        public string uf { get; set; }
        //        public string municipio { get; set; }
        //        public string logradouro { get; set; }
        //        public string complemento { get; set; }
        //        public string cep { get; set; }
        //        public string numero { get; set; }
        //        public string bairro { get; set; }
        //        public CnaePrincipal cnae_principal { get; set; }
    }
}