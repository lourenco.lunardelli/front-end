﻿using Domain;
using Infrastructure.Facades;
using Microsoft.Extensions.DependencyInjection;
using Models.Verweis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace BahnMagento
{
    class Program
    {
        static void Main()
        {
            Console.Title = "Bahn Magento";
            var watch = new Stopwatch();

            var services = new ServiceCollection();
            new InjectionFactory(services);

            var serviceProvider = services.BuildServiceProvider();

            try
            {
                watch.Restart();
                Console.WriteLine($"{DateTime.Now} | ###############################################");
                Console.WriteLine($"{DateTime.Now} | ##### bahn-magento - Magento cast process #####");
                Console.WriteLine($"{DateTime.Now} | ###############################################");

                List<NewEcommerceModel> ecommerceList = Bahngleis.HttpClient.GetAsync<NewEcommerceModel>().Result.Where(s => s.Platform.Name.ToUpper() == "MAGENTO").ToList();

                //List<NewEcommerceModel> ecommerceList = new List<NewEcommerceModel>
                //    {
                //        new NewEcommerceModel
                //        {
                //            Company = new NewCompanyModel
                //            {
                //                CompanyID = 1,
                //                Name = "Teste Company"
                //            },
                //            Name = "Teste Ecommerce",
                //            EcommerceID = 1,
                //            Platform = new NewPlatformModel
                //            {
                //                Name = "Magento",
                //                Password = "aaa",
                //                User = "bbb",
                //                PlatformID = 2,
                //                Url = "http://dockerized-magento.local"
                //            }
                //        }
                //    };

                if (ecommerceList.Count > 0)
                {
                    foreach (var ecommerce in ecommerceList)
                    {
                        try
                        {
                            Console.WriteLine($"{DateTime.Now} | {ecommerce.Name } => {Environment.GetEnvironmentVariable("SAP_DB")}");

                            new OrderService(serviceProvider).ImportOrdersToDb(serviceProvider, ecommerce);
                            //new OrderService().ImportOrdersToDb(ecommerce);

                            Console.WriteLine(string.Empty);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(DateTime.Now + " | " + ex.Message.ToString());
                        }

                    }

                    Thread.Sleep(5000);
                }

                Console.WriteLine(string.Empty);
                Console.WriteLine(DateTime.Now + " | bahn-magento synchronization run finished. Time elapsed: " + watch.Elapsed);
                Console.WriteLine(string.Empty);
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " | " + ex.ToString());
            }

        }
    }
}
