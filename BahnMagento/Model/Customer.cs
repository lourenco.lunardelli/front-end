﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BahnMagento.Model
{
    public class Customer
    {
        public string entity_id { get; set; }
        public string website_id { get; set; }
        public string email { get; set; }
        public string group_id { get; set; }
        public DateTime created_at { get; set; }
        public string disable_auto_group_change { get; set; }
        public string created_in { get; set; }
        public string prefix { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string suffix { get; set; }
        public string taxvat { get; set; }
        public string gender { get; set; }
        public string reward_update_notification { get; set; }
        public string reward_warning_notification { get; set; }
        public string dob { get; set; }
    }

}

