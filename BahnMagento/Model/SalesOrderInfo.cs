﻿namespace BahnMagento.Model
{

    public class SalesOrderInfo
    {
        public string entity_id { get; set; }
        public string status { get; set; }
        public string coupon_code { get; set; }
        public string shipping_description { get; set; }
        public string customer_id { get; set; }
        public string base_discount_amount { get; set; }
        public string base_grand_total { get; set; }
        public string base_shipping_amount { get; set; }
        public string base_shipping_tax_amount { get; set; }
        public string base_subtotal { get; set; }
        public string base_tax_amount { get; set; }
        public string base_total_paid { get; set; }
        public string base_total_refunded { get; set; }
        public string discount_amount { get; set; }
        public string grand_total { get; set; }
        public string shipping_amount { get; set; }
        public string shipping_tax_amount { get; set; }
        public string store_to_order_rate { get; set; }
        public string subtotal { get; set; }
        public string tax_amount { get; set; }
        public string total_paid { get; set; }
        public string total_refunded { get; set; }
        public string base_shipping_discount_amount { get; set; }
        public string base_subtotal_incl_tax { get; set; }
        public string base_total_due { get; set; }
        public string shipping_discount_amount { get; set; }
        public string subtotal_incl_tax { get; set; }
        public string total_due { get; set; }
        public string increment_id { get; set; }
        public string base_currency_code { get; set; }
        public string discount_description { get; set; }
        public string remote_ip { get; set; }
        public string store_currency_code { get; set; }
        public string store_name { get; set; }
        public string created_at { get; set; }
        public string shipping_incl_tax { get; set; }
        public string base_customer_balance_amount { get; set; }
        public string customer_balance_amount { get; set; }
        public string payment_method { get; set; }
        public string gift_message_from { get; set; }
        public string gift_message_to { get; set; }
        public string gift_message_body { get; set; }
        public string tax_name { get; set; }
        public string tax_rate { get; set; }
        public Address[] addresses { get; set; }
        public OrderItems[] order_items { get; set; }
        public OrderComments[] order_comments { get; set; }
    }

    public class Address
    {
        public string region { get; set; }
        public string postcode { get; set; }
        public string lastname { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string country_id { get; set; }
        public string firstname { get; set; }
        public string address_type { get; set; }
        public string prefix { get; set; }
        public string middlename { get; set; }
        public string suffix { get; set; }
        public string company { get; set; }
    }

    public class OrderItems
    {
        public string item_id { get; set; }
        public string parent_item_id { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
        public string qty_canceled { get; set; }
        public string qty_invoiced { get; set; }
        public string qty_ordered { get; set; }
        public string qty_refunded { get; set; }
        public string qty_shipped { get; set; }
        public string price { get; set; }
        public string base_price { get; set; }
        public string original_price { get; set; }
        public string base_original_price { get; set; }
        public string tax_percent { get; set; }
        public string tax_amount { get; set; }
        public string base_tax_amount { get; set; }
        public string discount_amount { get; set; }
        public string base_discount_amount { get; set; }
        public string row_total { get; set; }
        public string base_row_total { get; set; }
        public string price_incl_tax { get; set; }
        public string base_price_incl_tax { get; set; }
        public string row_total_incl_tax { get; set; }
        public string base_row_total_incl_tax { get; set; }
    }

    public class OrderComments
    {
        public string is_customer_notified { get; set; }
        public string is_visible_on_front { get; set; }
        public string comment { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
    }
}

