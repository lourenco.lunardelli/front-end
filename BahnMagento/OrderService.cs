﻿using BahnMagento.Model;
using Domain;
using Domain.Helper;
using Infrastructure.Facades;
using Models;
using Models.Verweis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BahnMagento
{
    public class OrderService : InjectionBase
    {

        public OrderService(IServiceProvider serviceProvider) : base(serviceProvider) { }

        public void ImportOrdersToDb(IServiceProvider serviceProvider, NewEcommerceModel ecommerce, string orderID = null)
        //public void ImportOrdersToDb(NewEcommerceModel ecommerce)
        {
            Console.WriteLine($"{DateTime.Now} | {ecommerce.Name} : STARTING to import the orders from Magento to bahn Database");

            //var ordersInBD = Bahngleis.HttpClient.GetAsync<NewOrderModel>(path: "Order/GetOrdersListDashboard").Result;

            var result = Get(ecommerce.Platform.Url + "/api/rest/orders?filter[1][attribute]=status&filter[1][eq]=processing");

            Dictionary<string, SalesOrderInfo> orders = JsonConvert.DeserializeObject<Dictionary<string, SalesOrderInfo>>(result);

            //var ordersToCast = orders.Select(s => s.increment_id).Except(ordersInBD.Where(s => s.Status.Name != "holded").Select(o => o.Number)).ToList();
            //var ordersToCast = new List<SalesOrderInfo>().Select(s => s.increment_id);

            if (orders != null && orders.Count() > 0)
            {
                int ordersImportedQtySuccess = 0;
                int ordersImportedQtyError = 0;
                int ordersNumbers = 0;
                var obj = new object();

                Console.WriteLine(DateTime.Now + " | Started the Cast from Magento orders into NewOrderModel object");

                List<NewOrderModel> listOrders = new List<NewOrderModel>();

                object sync = new object();

                Parallel.ForEach(orders,
                    new ParallelOptions { MaxDegreeOfParallelism = 4 },
                    order =>
                    {
                        lock (sync)
                        {
                            ordersNumbers++;
                            Console.Write($"{DateTime.Now} | {ordersNumbers} of {orders.Count()} | Casting Order: {order} ... " + Environment.NewLine);

                            try
                            {
                                //var orderValidation = ordersInBD.Where(s => s.Number == order.Value.increment_id).FirstOrDefault();

                                NewOrderModel NewOrderModel = CastToOrderModel(order.Value, ecommerce);

                                if (NewOrderModel != null)
                                {
                                    //NewOrderModel.Company = ecommerce;
                                    //NewOrderModel. = ecommerce.Name;
                                    NewOrderModel.EcommerceID = ecommerce.EcommerceID;
                                    // NewOrderModel.SAPCompany = ecommerce.SAPCompany;

                                    //if (orderValidation is null)
                                    //{
                                    //    var response = Bahngleis.HttpClient.PostAsyncNotDeserialized(NewOrderModel, "Order/SaveOrder_FromBahnMagento").Result;
                                    //    Console.WriteLine(response);
                                    //}
                                    //else
                                    //{

                                    //    obj = new
                                    //    {
                                    //        requeue = true
                                    //    };

                                    //    NewOrderModel.Status.Name = "processing";
                                    //    NewOrderModel.OrderID = orderValidation.OrderID;
                                    //    Bahngleis.HttpClient.PutAsync(NewOrderModel.OrderID, NewOrderModel, "Order/UpdateOrder", obj);
                                    //}
                                    ordersImportedQtySuccess++;
                                }
                                else
                                {

                                    Console.Write($"{DateTime.Now} | Casting Order: {order} | Order updated to OnHold in Magento" + Environment.NewLine);
                                    ordersImportedQtyError++;
                                }

                            }

                            catch (Exception exc)
                            {
                                Console.WriteLine($"{DateTime.Now} | Cast error " + exc);
                                ordersImportedQtyError++;
                            }
                        }

                    });

                Console.WriteLine($"{DateTime.Now} | Cast {ordersNumbers} orders finished. {ordersImportedQtySuccess} succeeded | {ordersImportedQtyError} with error");
            }
            else
            {
                Console.WriteLine(DateTime.Now + " | No order to Import");
            }


            Console.WriteLine(DateTime.Now + " | NewOrderModel: FINISHED to import the orders");

        }

        public NewOrderModel CastToOrderModel(SalesOrderInfo mageOrder, NewEcommerceModel ecommerce)
        {
            NewOrderModel order = new NewOrderModel();
            Customer mageCustomer = new Customer();

            try
            {
                try
                {
                    if (!string.IsNullOrEmpty(mageOrder.customer_id))
                    {
                        var result = Get(ecommerce.Platform.Url + "/api/rest/customer/" + mageOrder.customer_id);

                        mageCustomer = JsonConvert.
                           DeserializeObject<Customer>(result);
                    }
                }
                catch (WebException we)
                {

                    order = null;
                    Console.WriteLine(we.ToString());
                    throw we;
                }

                order.Number = mageOrder.increment_id;
                order.PurchaseDate = Convert.ToDateTime(mageOrder.created_at);
                order.ImportedDate = DateTime.Now;

                FillPayment(order, mageOrder);
                FillShipping(order, mageOrder);
                FillCustomerInfo(order, mageOrder, mageCustomer);
                FillPaymentAddress(order, mageOrder);
                FillShippingAddress(order, mageOrder);
                FillProduct(order, mageOrder);

                return order;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FillShipping(NewOrderModel order, SalesOrderInfo mageOrder)
        {

            // var ecommerceShipping = VerweisFacade.Repo.GetMethod(mageOrder.shipping_method, "api/method/shipping/getbyecommerce").Result;
            var ecommerceShipping = new EcommerceMethod();

            order.Shipping = new NewShippingModel
            {
                /// After it gets the Channel and Channel ID the system verifies
                /// if the order is coming from MercadoLivre If so, it checks its
                /// shipping model to get the shipping method and confirm if it
                /// is MercadoEnvios
                IsDropShipping = false,
                DueDate = DateTime.Today,
                Method = ecommerceShipping.EcommerceMethodID
            };

            if (mageOrder.shipping_amount != null)
            {
                order.Shipping.QuotedPrice = double.Parse(mageOrder.shipping_amount, System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        public void FillProduct(NewOrderModel order, SalesOrderInfo mageOrder)
        {
            NewProductModel product;
            order.Products = new List<NewProductModel>();

            foreach (var mageProduct in mageOrder.order_items)
            {
                if (string.IsNullOrEmpty(mageProduct.sku))
                {
                    throw new Exception("Custom: Erro ao obter informações do pedido: SKU em branco.");
                }

                double itemPrice = double.Parse(mageProduct.price, System.Globalization.CultureInfo.InvariantCulture);

                if (itemPrice <= 0)
                {
                    continue;
                }

                product = new NewProductModel
                {
                    Price = itemPrice,
                    Quantity = int.Parse(mageProduct.qty_ordered, System.Globalization.CultureInfo.InvariantCulture),
                    SKU = mageProduct.sku,
                    Name = mageProduct.name,
                    Discount = mageProduct.discount_amount == null ? 0 : Convert.ToDouble(mageProduct.discount_amount, System.Globalization.CultureInfo.InvariantCulture)

                };

                order.Products.Add(product);
            }
        }

        public void FillPaymentAddress(NewOrderModel order, SalesOrderInfo mageOrder)
        {

            var mageAddress = mageOrder.addresses.Where(s => s.address_type == "billing").FirstOrDefault();

            order.Payment.Address = new NewAddressModel
            {
                Name = mageAddress.firstname + " " + mageAddress.lastname,
                AddressType = "Pagamento",
                City = mageAddress.city,
                Country = mageAddress.country_id,
                TaxIdentification = order.Customer.TaxIdentification
                //IbgeCode = mageOrder.billing_address.ibge_code
            };

            FillStreetInfo(order.Payment.Address, mageAddress);
            FillAdditionalAddressInfo(order.Payment.Address, mageAddress);
        }

        public void FillShippingAddress(NewOrderModel order, SalesOrderInfo mageOrder)
        {
            var mageAddress = mageOrder.addresses.Where(s => s.address_type == "shipping").FirstOrDefault();


            order.Shipping.Address = new NewAddressModel
            {
                Name = mageAddress.firstname + " " + mageAddress.lastname,
                AddressType = "Entrega",
                City = mageAddress.city,
                Country = mageAddress.country_id,
                TaxIdentification = order.Customer.TaxIdentification

                //IbgeCode = mageOrder.shipping_address.ibge_code
            };

            FillStreetInfo(order.Shipping.Address, mageAddress);
            FillAdditionalAddressInfo(order.Shipping.Address, mageAddress);
        }

        public void FillStreetInfo(NewAddressModel orderAddress, Address mageOrder)
        {
            var street = mageOrder.street.Split('\n');

            switch (street.Count())
            {
                case 1:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = "0";
                    orderAddress.Complement = "";
                    orderAddress.District = "S/B";
                    break;

                case 2:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = street[1].Trim();
                    orderAddress.District = "S/B";
                    break;

                case 3:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = street[1].Trim();
                    orderAddress.District = street[2].Trim();
                    break;

                case 4:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = street[1].Trim();
                    orderAddress.Complement = street[2].Trim();
                    orderAddress.District = street[3].Trim();
                    break;
            }
        }

        public void FillAdditionalAddressInfo(NewAddressModel orderAddress, Address mageOrder)
        {
            if (!string.IsNullOrEmpty(mageOrder.postcode))
            {
                orderAddress.ZipCode = StringUtils.RemoveSpecialCharacters(mageOrder.postcode).Trim();
            }

            orderAddress.UF = mageOrder.region;

            if (!string.IsNullOrEmpty(mageOrder.telephone))
            {
                orderAddress.Phone = StringUtils.RemoveSpecialCharacters(mageOrder.telephone);

                if (orderAddress.Phone.StartsWith("0"))
                {
                    orderAddress.Phone = orderAddress.Phone.Substring(2);
                }
            }
        }

        public void FillCustomerInfo(NewOrderModel order, SalesOrderInfo mageOrder, Customer mageCustomer)
        {
            var mageBillingAddress = mageOrder.addresses.Where(s => s.address_type == "billing").FirstOrDefault();

            order.Customer = new NewCustomerModel
            {
                CustomerID = string.IsNullOrEmpty(mageOrder.customer_id) ? 0 : int.Parse(mageOrder.customer_id),
                TaxIdentification = mageCustomer.taxvat
            };

            if (!string.IsNullOrEmpty(mageCustomer.firstname))
            {
                order.Customer.Name = mageCustomer.firstname + " " + mageCustomer.lastname;
            }
            else
            {
                var mageAddress = mageOrder.addresses.Where(s => s.address_type == "shipping").FirstOrDefault();

                order.Customer.Name = mageAddress.firstname + " " + mageAddress.lastname;
            }

            order.Customer.Email = mageCustomer.email;

            if (!string.IsNullOrEmpty(mageBillingAddress.telephone))
            {
                order.Customer.PhoneNumber = StringUtils.RemoveSpecialCharacters(mageBillingAddress.telephone);

                if (order.Customer.PhoneNumber.StartsWith("0"))
                {
                    order.Customer.PhoneNumber = order.Customer.PhoneNumber.Substring(2);
                }
            }

            order.Customer.Address = new NewAddressModel();
            order.Customer.Address = order.Payment.Address;
            order.Customer.Address.AddressType = "Cliente";
        }

        public void FillPayment(NewOrderModel order, SalesOrderInfo mageOrder)
        {
            var ecommercePayment = Verweis.HttpClient.GetAsync<EcommerceMethod>(path: "api/method/shipping/getbyecommerce/" + mageOrder.payment_method).Result.First();


            order.Payment = new NewPaymentModel
            {
                //PaymentID = Convert.ToInt32(mageOrder.payment.payment_id),
                Method = ecommercePayment.EcommerceMethodID
            };

            order.Payment.Currency = mageOrder.base_currency_code;
            order.Payment.Discount = double.Parse(mageOrder.discount_amount, System.Globalization.CultureInfo.InvariantCulture);
            order.Payment.Total = double.Parse(mageOrder.grand_total, System.Globalization.CultureInfo.InvariantCulture);
            order.Payment.DueDate = DateTime.Today;

            //if (int.TryParse(mageOrder.payment.installments, out int installments))
            //{
            //    order.Payment.Installments = installments;
            //}
        }

        public string UrlEncodeRelaxed(string value)
        {
            string[] uriRfc3986CharsToEscape = { "!", "*", "'", "(", ")", "?" };
            string[] uriRfc3968EscapedHex = { "%21", "%2A", "%27", "%28", "%29", "%26" };

            var escaped = new StringBuilder(Uri.EscapeDataString(value));

            for (var i = 0; i < uriRfc3986CharsToEscape.Length; i++)
            {
                var t = uriRfc3986CharsToEscape[i];
                escaped.Replace(t, uriRfc3968EscapedHex[i]);
            }

            return escaped.ToString();
        }

        public string GetHash(string baseString, string key)
        {
            var crypto = new HMACSHA1
            {
                Key = Encoding.UTF8.GetBytes(key)
            };
            return UrlEncodeRelaxed(HashWith(baseString, crypto));
        }

        public static string GetTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds).ToString();
        }

        public static string HashWith(string input, HashAlgorithm algorithm)
        {
            byte[] data = Encoding.UTF8.GetBytes(input);
            byte[] hash = algorithm.ComputeHash(data);

            return Convert.ToBase64String(hash);
        }

        public string Get(string Url)
        {
            var request = GenerateRequest("GET", Url, "f487eb154925f4fe4e000ef5db2fb453", "eebd05e876804735f253dd11e80775d7", "95fff4cb04fa4d3349782b7ab75be6e5", "d7f4b5bdcd728d54df4a553ab80f2a8f", "HMAC-SHA1", "1.0");

            try
            {
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                string result = string.Empty;
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }

                return result;
            }
            catch (WebException ex)
            {
                if (ex.Message.Contains("404"))
                {
                    return "{\"status\"Not Found\"}";
                }
                else
                {
                    throw ex;
                }
            }
        }

        public HttpWebRequest GenerateRequest(string method, string url, string accessToken, string tokenSecret, string consumerKey, string consumerSecret, string signatureMethod, string version)
        {
            var nonce = Guid.NewGuid().ToString("N").Substring(0, 16);
            var timestamp = GetTimestamp(DateTime.Now);

            var baseUrl = UrlEncodeRelaxed(url);
            var baseParameters = UrlEncodeRelaxed(
                   $"oauth_consumer_key={consumerKey}" +
                   $"&oauth_nonce={nonce}" +
                   $"&oauth_signature_method={signatureMethod}" +
                   $"&oauth_timestamp={timestamp}" +
                   $"&oauth_token={accessToken}" +
                   $"&oauth_version={version}");


            var baseString = $"{method}&{baseUrl}&{baseParameters}";
            var key = $"{consumerSecret}&{tokenSecret}";

            var signature = GetHash(baseString, key);

            var http = (HttpWebRequest)WebRequest.Create(new Uri(url));


            http.ContentType = "application/json";
            http.Accept = "application/json";
            http.Headers.Add("Accept-Encoding", "gzip, deflate");
            http.Headers.Add("Authorization", $"OAuth oauth_consumer_key=\"{consumerKey}\"," +
                                 $"oauth_nonce=\"{nonce}\"," +
                                 $"oauth_signature=\"{signature}\"," +
                                 $"oauth_signature_method=\"{signatureMethod}\"," +
                                 $"oauth_timestamp=\"{timestamp}\"," +
                                 $"oauth_token=\"{accessToken}\"," +
                                 $"oauth_version=\"{version}\"");
            http.Method = method;

            return http;
        }
    }
}
