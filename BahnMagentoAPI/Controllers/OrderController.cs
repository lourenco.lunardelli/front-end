﻿using BahnMagentoAPI.Magento.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BahnMagentoAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IServiceProvider _serviceProvider;

        public OrderController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        [HttpGet("CastOrder")]
        public async Task<ActionResult> CastOrder(int ecommerceID, string incrementID)
        {
            try
            {
                IMageFacade magento = (IMageFacade)_serviceProvider.GetService(typeof(IMageFacade));
                return Ok(await magento.Services[ecommerceID].CastOrder(incrementID));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("HoldOrder")]
        [HttpPatch("HoldOrder")]
        public ActionResult HoldOrder([FromBody] int ecommerceID, string incrementID, string comment, bool notify)
        {
            try
            {
                IMageFacade magento = (IMageFacade)_serviceProvider.GetService(typeof(IMageFacade));
                magento.Services[ecommerceID].HoldOrder(incrementID, comment, notify);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("AddComment")]
        public async Task<ActionResult> AddComment([FromBody] int ecommerceID, string incrementID, string status, string comment, bool notify)
        {
            try
            {
                IMageFacade magento = (IMageFacade)_serviceProvider.GetService(typeof(IMageFacade));
                await magento.Services[ecommerceID].AddComment(incrementID, status, comment, notify);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetOrdersByStatus")]
        public async Task<ActionResult> GetOrdersByStatus([FromQuery] int ecommerceID, string status)
        {
            try
            {
                IMageFacade magento = (IMageFacade)_serviceProvider.GetService(typeof(IMageFacade));
                var a = await magento.Repos[ecommerceID].GetOrders(status);
                return Ok(a);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetOrderByNumber")]
        public async Task<ActionResult> GetOrderByNumber([FromQuery] int ecommerceID, string incrementID)
        {
            try
            {
                IMageFacade magento = (IMageFacade)_serviceProvider.GetService(typeof(IMageFacade));
                return Ok(await magento.Repos[ecommerceID].GetOrderByNumber(incrementID));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
