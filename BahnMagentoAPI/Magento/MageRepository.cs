﻿using BahnMagentoAPI.Magento.Interfaces;
using Infrastructure.Clients.Magento;
using Infrastructure.Clients.Magento.ServiceReference;
using Models.Verweis;
using System;
using System.Threading.Tasks;

namespace BahnMagentoAPI.Magento
{
    public class MageRepository : IMageRepository
    {
        private readonly MageConnection _connection;

        public MageRepository(NewEcommerceModel ecommerce)
        {
            _connection = new MageConnection(ecommerce);
        }

        public async Task<salesOrderListEntity[]> GetOrders(string status)
        {
            complexFilter[] cFilter = new complexFilter[]{

                    new complexFilter{
                        key = "status",
                        value = new associativeEntity{
                            key = "in",
                            value = status,
                        }
                    }
                };

            var filters = new filters
            {
                complex_filter = cFilter
            };

            return await _connection.MageService.salesOrderListAsync(_connection.Session, filters);
        }

        public async Task<salesOrderEntity> GetOrderByNumber(string incrementID)
        {
            return await _connection.MageService.salesOrderInfoAsync(_connection.Session, incrementID);
        }

        public async Task<salesOrderStatusHistoryEntity[]> GetOrderStatusHistory(string incrementID)
        {
            var orderInfo = await _connection.MageService.salesOrderInfoAsync(_connection.Session, incrementID);
            return orderInfo.status_history;
        }

        public async Task<customerCustomerEntity> CustomerCustomerInfoAsync(int CustomerID)
        {
            return await _connection.MageService.customerCustomerInfoAsync(_connection.Session, CustomerID, null);
        }


        //public async Task UpdateOrderStatus()
        //{

        //    var queryParams = new
        //    {
        //        dataIni = DateTime.Now.AddDays(-15).Date,
        //        ecommerceStatus = new List<string> { "processing", "integrated" }
        //    };

        //    List<NewOrderModel> orders = Infrastructure.Facades.Bahngleis.HttpClient.GetAsync<NewOrderModel>(queryParams, "order/GetOrdersListDashboard").Result;

        //    foreach (var order in orders)
        //    {
        //        try
        //        {
        //            var Danfe = OrderFacade.SAPRepo.GetDANFE(order.OrderNumber);
        //            var SAPDocuments = DocumentsFacade.Repo.GetDocumentsSAP(order.OrderNumber);

        //            if (Danfe.DocNum != 0 && SAPDocuments.U_Status_WMS == "DESPACHADO COMPLETO")
        //            {
        //                var docNum = Danfe.DocNum.ToString();
        //                var accessKey = Danfe.U_ChaveAcesso;
        //                var notaFiscal = Danfe.Serial;

        //                var query = new
        //                {
        //                    order.OrderID
        //                };

        //                var orderSQL = Infrastructure.Facades.Bahngleis.HttpClient.GetByQueryAsync<NewOrderModel>(query, "Order/GetOrderById").Result;

        //                if (orderSQL.Ecommerce.Platform.Name == "MAGENTO")
        //                {
        //                    await UpdateOrderMagento(orderSQL, Danfe, docNum, accessKey, notaFiscal);
        //                }

        //                orderSQL.EcommerceStatus = "complete";
        //                orderSQL.NFKey = notaFiscal;
        //                Infrastructure.Facades.Bahngleis.HttpClient.PutAsync(order.OrderID, orderSQL, "order/UpdateOrder").Wait();
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            continue;
        //        }
        //    };
        //}

        //public async Task UpdateOrderMagento(NewOrderModel order, Danfe orderDetails, string docNum, string accessKey, string notaFiscal)
        //{
        //    string trackingNo;
        //    bool isOrderUpdated = false;
        //    bool isMeliOrder = false;

        //    var statusHistory = OrderFacade.MageRepo.GetOrderStatusHistory(order.Ecommerce, order.OrderNumber);

        //    /// After it gets the status history it checks if the order already
        //    /// has the Chave de Acesso and if this order was imported from MercadoLivre
        //    for (int i = 0; i < statusHistory.Length; i++)
        //    {
        //        if (!String.IsNullOrEmpty(statusHistory[i].comment))
        //        {
        //            if (statusHistory[i].comment.Contains("Chave de Acesso:"))
        //            {
        //                isOrderUpdated = true;
        //            }

        //            if (statusHistory[i].comment.Contains("MERCADO_LIVRE"))
        //            {
        //                isMeliOrder = true;
        //            }
        //            else if (statusHistory[i].comment.Contains("Mercadolivre"))
        //            {
        //                isMeliOrder = true;
        //            }

        //            /// When it confirms that it has already the Chave the Acesso
        //            /// in the order history and the order came from MercadoLivre
        //            /// it breaks the loop
        //            if (isOrderUpdated && isMeliOrder)
        //            {
        //                break;
        //            }
        //        }
        //    }

        //    /// If the order came from MercadoLivre and already has the Chave de
        //    /// Acesso it means that for some reason its status was changed
        //    /// incorrectly. Then, the system forces to change back to complete,
        //    /// otherwise, SkyHub will not get the details to send to the marketplaces
        //    if (isOrderUpdated)
        //    {
        //        AddComment(order.Ecommerce, order.OrderNumber, "Complete", "m.one: Alterado pelo sistema").Wait();
        //    }
        //    else
        //    {
        //        string notify = "1";

        //        //If is pickup store, do not notify
        //        //TODO: check if this is needed to Mercado Livre pickup store
        //        if (order.Shipping.Method.Equals("flatrate_flatrate"))
        //        {
        //            trackingNo = "Retirada Local";
        //            notify = "0";
        //        }
        //        else
        //        {
        //            var meTracking = await MercadoLivreFacade.Repo.GetMETracking(orderDetails.OrderEntry);
        //            trackingNo = meTracking?.U_Tracking;
        //        }


        //        if (!string.IsNullOrEmpty(trackingNo) && !string.IsNullOrEmpty(accessKey))
        //        {
        //            string comment = "Chave de Acesso: " + accessKey;

        //            /// TODO: Add the other e-mails to filter here. It will prevent Magento to send e-mail notifications when orders get updated
        //            if (order.Customer.Email.Contains("NAO_INFORMADO.NAO.BR") || order.Customer.Email.Contains("mktp.extra.com.br"))
        //            {
        //                notify = "0";
        //            }
        //            else if (order.Shipping.Method == "correios_9" || order.Shipping.Method == "correios_3"
        //                  || order.Shipping.Method == "correios_09" || order.Shipping.Method == "correios_03")
        //            {
        //                comment += " (Nota Fiscal) </br>" +
        //                           "Você pode consultá-la em: <a href = 'https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' target = '_blank'> https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8= </a>";
        //            }
        //            else if (order.Shipping.Method == "correios_14" || order.Shipping.Method == "correios_15")
        //            {
        //                comment += " (Nota Fiscal) </br>" +
        //                           "Você pode consultá-la em: <a href = 'https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' target = '_blank'> https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8= </a>" +
        //                           "<br/><br/>" +
        //                           "Nota fiscal: " + notaFiscal +
        //                           "<br/><br/>" +
        //                           "Lembramos que será necessário aguardar o prazo de até 24 horas para que o rastreio esteja atualizado no sistema da Jamef." +
        //                           "<br/><br/>" +
        //                           "O rastreamento pode ser feito diretamente no site: http://www.jamef.com.br/web/servicos/rastreamento/consulta_portal.asp";
        //            }
        //            else if (order.Shipping.Method == "correios_16" || order.Shipping.Method == "correios_17")
        //            {
        //                comment += " (Nota Fiscal) </br>" +
        //                           "Você pode consultá-la em: <a href = 'https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' target = '_blank'> https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8= </a>" +
        //                           "<br/><br/>" +
        //                           "Nota fiscal: " + notaFiscal +
        //                           "<br/><br/>" +
        //                           "Lembramos que será necessário aguardar o prazo de até 24 horas para que o rastreio esteja atualizado no sistema da Braspress." +
        //                           "<br/><br/>" +
        //                           "O rastreamento pode ser feito diretamente no site: https://www.braspress.com.br/w/tracking/search?cnpj=&numero=18828&documentType=NOTAFISCAL";
        //            }
        //            else if (order.Shipping.Method == "correios_21")
        //            {
        //                comment += " (Nota Fiscal) </br>" +
        //                              "Você pode consultá-la em: <a href = 'https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' target = '_blank'> https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8= </a>" +
        //                              "<br/><br/>" +
        //                              "Nota fiscal: " + notaFiscal +
        //                              "<br/><br/>" +
        //                              "Lembramos que será necessário aguardar o prazo de até 24 horas para que o rastreio esteja atualizado no sistema da Total Express." +
        //                              "<br/><br/>" +
        //                              "O rastreamento pode ser feito diretamente no site: http://tracking.totalexpress.com.br/tracking/0?cpf_cnpj=";
        //            }
        //            else if (order.Shipping.Method == "correios_22")
        //            {
        //                comment += $" (Nota Fiscal) </br>" +
        //                              "Você pode consultá-la em: <a href = 'https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' target = '_blank'> https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8= </a>" +
        //                              "<br/><br/>" +
        //                              "Nota fiscal: " + notaFiscal +
        //                              "<br/><br/>" +
        //                              "Lembramos que será necessário aguardar o prazo de até 24 horas para que o rastreio esteja atualizado no sistema da TNT." +
        //                              "<br/><br/>" +
        //                              "O rastreamento pode ser feito através do link: <a href='http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=11027350000168&nota=" + notaFiscal + "' target = '_blank'>Rastreamento TNT Brasil</a>" +
        //                              "<br/>" +
        //                              "Ou pelo site: <a href='http://radar.tntbrasil.com.br/radar/public/localizacaoSimplificada'>http://radar.tntbrasil.com.br/radar/public/localizacaoSimplificada</a>";
        //            }
        //            else
        //            {
        //                comment += " (Nota Fiscal) </br>" +
        //                    "Você pode consultá-la em: <a href = 'https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' target = '_blank'> https://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8= </a>" +
        //                    "<br/><br/>" +
        //                    "Código de Rastreio: " + trackingNo +
        //                    "<br/><br/>" +
        //                    "Lembramos que será necessário aguardar o prazo de até 24 horas para que o rastreio esteja atualizado no sistema dos Correios." +
        //                    "<br/><br/>" +
        //                    "O rastreamento pode ser feito diretamente no site: http://www.correios.com.br";
        //            }

        //            var mageOrder = OrderFacade.MageRepo.GetOrderById(order.Ecommerce, order.OrderNumber);

        //            AddComment(order.Ecommerce, order.OrderNumber, mageOrder.status, comment, notify).Wait();
        //            AddComment(order.Ecommerce, order.OrderNumber, mageOrder.status, "Pedido SAP: " + docNum, null).Wait();
        //            OrderFacade.MagentoShippingRepo.CreateShipping(order.Ecommerce, order.OrderNumber, "m.One: Pedido enviado", trackingNo, int.Parse(notify));
        //        }
        //    }
        //}

    }
}
