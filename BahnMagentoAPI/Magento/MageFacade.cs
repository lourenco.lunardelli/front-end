﻿using BahnMagentoAPI.Magento.Interfaces;
using Domain.Verweis;
using Domain.Verweis.Interfaces;
using Models.Verweis;
using System;
using System.Collections.Generic;

namespace BahnMagentoAPI.Magento
{
    public class MageFacade : IMageFacade
    {
        private Dictionary<int, IMageService> _services;
        private Dictionary<int, IMageRepository> _repos;

        private readonly IServiceProvider _serviceProvider;
        private List<NewEcommerceModel> _ecommerces;

        public MageFacade(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ecommerces = new List<NewEcommerceModel> {

                new NewEcommerceModel
                {
                    Company = new NewCompanyModel
                    {
                        CompanyID = 1,
                        Name = "g2w"
                    },
                    EcommerceID = 1,
                    Name = "teste",
                    Password = "password123",
                    Platform = new NewPlatformModel
                    {
                        PlatformID = 1,
                        Name = "Magento"
                    },
                    Url = "https://docker-magento.bringit.com.br/api/v2_soap",
                    User = "bahn"
                }
             };
        }

        //    var VerweisFacade = (IVerweisFacade)_serviceProvider.GetService(typeof(IVerweisFacade));
        //    _ecommerces = VerweisFacade.Repo.GetEcommerces().Result;
        //}

        public Dictionary<int, IMageService> Services
        {
            get
            {
                if (_services is null || _services.Count == 0)
                {
                    _services = new Dictionary<int, IMageService>();
                    foreach (var ecommerce in _ecommerces)
                        _services.Add(ecommerce.EcommerceID, new MageService(ecommerce));
                }
                return _services;
            }
        }

        public Dictionary<int, IMageRepository> Repos
        {
            get
            {
                if (_repos is null || _repos.Count == 0)
                {
                    _repos = new Dictionary<int, IMageRepository>();

                    foreach (var ecommerce in _ecommerces)
                        _repos.Add(ecommerce.EcommerceID, new MageRepository(ecommerce));
                }
                return _repos;
            }
        }
    }
}
