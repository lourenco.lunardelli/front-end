﻿using BahnMagentoAPI.Magento.Interfaces;
using Domain.Helper;
using Infrastructure.Clients.Magento;
using Infrastructure.Clients.Magento.ServiceReference;
using Infrastructure.Facades;
using Models;
using Models.Verweis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BahnMagentoAPI.Magento
{
    public class MageService : IMageService
    {
        private readonly MageConnection _connection;
        public MageService(NewEcommerceModel ecommerce)
        {
            _connection = new MageConnection(ecommerce);
        }

        public async Task<bool> AddComment(string incrementId, string status, string comment, bool notify = true)
        {
            bool result = false;

            if (Environment.GetEnvironmentVariable("ENVIRONMENT") != "stage")
            {
                result = await _connection.MageService.salesOrderAddCommentAsync(_connection.Session, incrementId, status, HttpUtility.HtmlEncode(comment), notify ? "1" : "0");
            }
            return result;
        }

        public async Task HoldOrder(string incrementId, string comment, bool notify = true)
        {
            if (Environment.GetEnvironmentVariable("ENVIRONMENT") != "stage")
            {
                await _connection.MageService.salesOrderHoldAsync(_connection.Session, incrementId);
                await _connection.MageService.salesOrderAddCommentAsync(_connection.Session, incrementId, "holded", HttpUtility.HtmlEncode(comment), notify ? "1" : "0");
            }
        }

        public async Task<int> CreateShipping(string incremementId, string comment, string trackingNumber, int notify)
        {
            if (Environment.GetEnvironmentVariable("ENVIRONMENT") != "stage")
            {
                try
                {
                    string shippingId = await _connection.MageService.salesOrderShipmentCreateAsync(_connection.Session, incremementId, null, HttpUtility.HtmlEncode(comment), notify, notify);
                    return await _connection.MageService.salesOrderShipmentAddTrackAsync(_connection.Session, shippingId, "correios", "Correios", trackingNumber);
                }
                catch (Exception)
                {
                    await _connection.MageService.salesOrderAddCommentAsync(_connection.Session, incremementId, "complete", "Error to create shipping <br> Updated by Bahn", null);
                }
            }

            return 0;
        }

        //------------------------------------CAST ORDER------------------------------------
        public async Task<NewOrderModel> CastOrder(string mageOrderNumber)
        {
            customerCustomerEntity mageCustomer = new customerCustomerEntity();

            salesOrderEntity orderMagento = await _connection.MageService.salesOrderInfoAsync(_connection.Session, mageOrderNumber);

            if (!string.IsNullOrEmpty(orderMagento.customer_id))
                mageCustomer = await _connection.MageService.customerCustomerInfoAsync(_connection.Session, Convert.ToInt32(orderMagento.customer_id), null);


            NewOrderModel order = new NewOrderModel
            {
                Number = orderMagento.increment_id,
                PurchaseDate = Convert.ToDateTime(orderMagento.created_at),
                ImportedDate = DateTime.Now
            };

            FillPayment(order, orderMagento);
            FillShipping(order, orderMagento);
            FillCustomerInfo(order, orderMagento, mageCustomer);
            FillPaymentAddress(order, orderMagento);
            FillShippingAddress(order, orderMagento);
            FillProduct(order, orderMagento);

            return order;
        }

        public void FillShipping(NewOrderModel order, salesOrderEntity mageOrder)
        {
            //TODO: get shipping method na verweis
            // var ecommerceShipping = VerweisFacade.Repo.GetMethod(mageOrder.shipping_method, "api/method/shipping/getbyecommerce").Result;
            var ecommerceShipping = new MappedMethod();

            order.Shipping = new NewShippingModel
            {
                IsDropShipping = false,
                DueDate = DateTime.Today,
                Method = ecommerceShipping.MappedMethodID,
                QuotedPrice = !string.IsNullOrEmpty(mageOrder.shipping_amount) ? double.Parse(mageOrder.shipping_amount, System.Globalization.CultureInfo.InvariantCulture) : 0
            };
        }

        public void FillProduct(NewOrderModel order, salesOrderEntity mageOrder)
        {
            order.Products = new List<NewProductModel>();

            foreach (var mageProduct in mageOrder.items)
            {
                if (string.IsNullOrEmpty(mageProduct.sku))
                    throw new Exception($"Erro ao obter informações do pedido: SKU em branco do produto {mageProduct.name}.");

                double itemPrice = double.Parse(mageProduct.price, System.Globalization.CultureInfo.InvariantCulture);

                if (itemPrice <= 0)
                    throw new Exception($"Valor do produto {mageProduct.sku} zerado.");

                NewProductModel product = new NewProductModel
                {
                    Price = itemPrice,
                    Quantity = int.Parse(mageProduct.qty_ordered, System.Globalization.CultureInfo.InvariantCulture),
                    SKU = mageProduct.sku,
                    Name = mageProduct.name,
                    Discount = mageProduct.discount_amount == null ? 0 : Convert.ToDouble(mageProduct.discount_amount, System.Globalization.CultureInfo.InvariantCulture)
                };

                order.Products.Add(product);
            }
        }

        public void FillPaymentAddress(NewOrderModel order, salesOrderEntity mageOrder)
        {

            var mageAddress = mageOrder.billing_address;

            order.Payment.Address = new NewAddressModel
            {
                Name = mageAddress.firstname + " " + mageAddress.lastname,
                AddressType = "Pagamento",
                City = mageAddress.city,
                Country = mageAddress.country_id,
                TaxIdentification = order.Customer.TaxIdentification
                //IbgeCode = mageOrder.billing_address.ibge_code
            };

            FillStreetInfo(order.Payment.Address, mageAddress);
            FillAdditionalAddressInfo(order.Payment.Address, mageAddress);
        }

        public void FillShippingAddress(NewOrderModel order, salesOrderEntity mageOrder)
        {
            var mageAddress = mageOrder.shipping_address;


            order.Shipping.Address = new NewAddressModel
            {
                Name = mageAddress.firstname + " " + mageAddress.lastname,
                AddressType = "Entrega",
                City = mageAddress.city,
                Country = mageAddress.country_id,
                TaxIdentification = order.Customer.TaxIdentification

                //IbgeCode = mageOrder.shipping_address.ibge_code
            };

            FillStreetInfo(order.Shipping.Address, mageAddress);
            FillAdditionalAddressInfo(order.Shipping.Address, mageAddress);
        }

        public void FillStreetInfo(NewAddressModel orderAddress, salesOrderAddressEntity mageOrder)
        {
            var street = mageOrder.street.Split('\n');

            switch (street.Count())
            {
                case 1:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = "0";
                    orderAddress.Complement = "";
                    orderAddress.District = "S/B";
                    break;

                case 2:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = street[1].Trim();
                    orderAddress.District = "S/B";
                    break;

                case 3:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = street[1].Trim();
                    orderAddress.District = street[2].Trim();
                    break;

                case 4:
                    orderAddress.Street = street[0].Trim();
                    orderAddress.Number = street[1].Trim();
                    orderAddress.Complement = street[2].Trim();
                    orderAddress.District = street[3].Trim();
                    break;
            }
        }

        public void FillAdditionalAddressInfo(NewAddressModel orderAddress, salesOrderAddressEntity mageOrder)
        {
            if (!string.IsNullOrEmpty(mageOrder.postcode))
                orderAddress.ZipCode = StringUtils.RemoveSpecialCharacters(mageOrder.postcode).Trim();

            orderAddress.UF = mageOrder.region;

            if (!string.IsNullOrEmpty(mageOrder.telephone))
            {
                orderAddress.Phone = StringUtils.RemoveSpecialCharacters(mageOrder.telephone);

                if (orderAddress.Phone.StartsWith("0"))
                    orderAddress.Phone = orderAddress.Phone.Substring(2);
            }
        }

        public void FillCustomerInfo(NewOrderModel order, salesOrderEntity mageOrder, customerCustomerEntity mageCustomer)
        {
            var mageBillingAddress = mageOrder.billing_address;

            order.Customer = new NewCustomerModel
            {
                CustomerID = string.IsNullOrEmpty(mageOrder.customer_id) ? 0 : int.Parse(mageOrder.customer_id),
                TaxIdentification = mageCustomer.taxvat
            };

            if (!string.IsNullOrEmpty(mageCustomer.firstname))
                order.Customer.Name = mageCustomer.firstname + " " + mageCustomer.lastname;
            else
                order.Customer.Name = mageOrder.shipping_address.firstname + " " + mageOrder.shipping_address.lastname;

            order.Customer.Email = mageCustomer.email;

            if (!string.IsNullOrEmpty(mageBillingAddress.telephone))
            {
                order.Customer.PhoneNumber = StringUtils.RemoveSpecialCharacters(mageBillingAddress.telephone);

                if (order.Customer.PhoneNumber.StartsWith("0"))
                    order.Customer.PhoneNumber = order.Customer.PhoneNumber.Substring(2);
            }

            order.Customer.Address = new NewAddressModel();
            order.Customer.Address = order.Payment.Address;
            order.Customer.Address.AddressType = "Cliente";
        }

        public void FillPayment(NewOrderModel order, salesOrderEntity mageOrder)
        {
            //TODO: get payment method na verweis
            var ecommercePayment = Verweis.HttpClient.GetAsync<MappedMethod>(path: "api/method/shipping/getbyecommerce/" + mageOrder.payment.method).Result.First();
            order.Payment = new NewPaymentModel
            {
                Method = ecommercePayment.EcommerceName,
                Currency = mageOrder.base_currency_code,
                Discount = double.Parse(mageOrder.discount_amount, System.Globalization.CultureInfo.InvariantCulture),
                Total = double.Parse(mageOrder.grand_total, System.Globalization.CultureInfo.InvariantCulture),
                DueDate = DateTime.Today
            };

            //if (int.TryParse(mageOrder.payment.installments, out int installments))
            //{
            //    order.Payment.Installments = installments;
            //}
        }
    }
}
