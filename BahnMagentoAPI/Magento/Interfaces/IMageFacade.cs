﻿using System.Collections.Generic;

namespace BahnMagentoAPI.Magento.Interfaces
{
    public interface IMageFacade
    {
        Dictionary<int, IMageService> Services { get; }
        Dictionary<int, IMageRepository> Repos { get; }
    }
}