﻿using Infrastructure.Clients.Magento.ServiceReference;
using Models.Verweis;
using System;
using System.Threading.Tasks;

namespace BahnMagentoAPI.Magento.Interfaces
{
    public interface IMageRepository
    {
        Task<salesOrderEntity> GetOrderByNumber(string OrderNumber);

        Task<salesOrderListEntity[]> GetOrders(string status);

        Task<salesOrderStatusHistoryEntity[]> GetOrderStatusHistory(string incremementId);

        Task<customerCustomerEntity> CustomerCustomerInfoAsync(int CustomerID);
    }
}