﻿using Models;
using System.Threading.Tasks;

namespace BahnMagentoAPI.Magento.Interfaces
{
    public interface IMageService
    {
        Task<bool> AddComment(string incrementId, string status, string comment, bool notify = true);

        Task HoldOrder(string incrementId, string comment, bool notify = true);

        Task<int> CreateShipping(string incremementId, string comment, string trackingNumber, int notify);

        Task<NewOrderModel> CastOrder(string mageOrderNumber);
    }
}